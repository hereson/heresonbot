﻿using HeresonBot.DataClasses.Users;
using System.Collections.Generic;

namespace HeresonBot.Services.Party
{
    class PartyService
    {
        public List<PartyUser> UserList { get; } = new List<PartyUser>();
        public PartyUser Host { get; private set; }

        public PartyService(PartyUser host)
        {
            SetNewHost(host);
            AddUser(host);
        }

        /// <summary>
        /// Sets new host.
        /// </summary>
        /// <param name="user">User object.</param>
        public void SetNewHost(PartyUser user)
        {
            Host = user;
        }

        /// <summary>
        /// Finds User in the current party.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <returns>User object.</returns>
        public PartyUser FindUser(ulong id)
        {
            foreach (PartyUser user in UserList)
                if (user.DiscordId == id)
                    return user;
            return null;
        }

        /// <summary>
        /// Adds User to the current party.
        /// </summary>
        /// <param name="user">User object.</param>
        /// <returns>Boolean.</returns>
        public bool AddUser(PartyUser user)
        {
            if (user.InParty)
                return false;
            user.JoinParty(this);
            UserList.Add(user);
            return true;
        }

        /// <summary>
        /// Removes User from the current party.
        /// </summary>
        /// <param name="user">User object.</param>
        /// <returns>Boolean.</returns>
        public bool RemoveUser(PartyUser user)
        {
            user.LeaveParty();
            return UserList.Remove(user);
        }

        /// <summary>
        /// Removes every User from the party.
        /// </summary>
        public void Disband()
        {
            for (int i = 0; i < Count; i++)
                UserList[i].LeaveParty();
            UserList.Clear();
        }

        /// <summary>
        /// Returns the number of users in the party.
        /// </summary>
        public int Count { get { return UserList.Count; } }
    }
}