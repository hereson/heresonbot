﻿using System;
using System.Timers;

namespace HeresonBot.Services.Discord
{
    class TimeService
    {
        Timer _timer = new Timer();

        public TimeService()
        {
            DoTask();
        }

        public void DoTask()
        {
            _timer.AutoReset = true;
            _timer.Enabled = true;
            _timer.Interval = GetFirstInterval();

            _timer.Elapsed += (sender, e) => OnTimedEvent(sender, e, "testparameter");
            _timer.Start();
            Console.WriteLine($"Event will be invoked in {_timer.Interval} milliseconds.");
        }

        /// <summary>
        /// Get the time in milliseconds until the next hour.
        /// </summary>
        /// <returns>Milliseconds until next hour.</returns>
        private  int GetFirstInterval()
        {
            return (59 - DateTime.Now.Minute) * 1000 * 60 + (60 - DateTime.Now.Second) * 1000;
        }

        /// <summary>
        /// Called when the time has elapsed.
        /// </summary>
        /// <param name="source">Object source.</param>
        /// <param name="e">Arguments</param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e, string s)
        {
            Console.WriteLine($"Event invoked at {e.SignalTime}.");
            _timer.Interval = 3600000; // Set the next event to be exactly in one hour.
        }
    }
}