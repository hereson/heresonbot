﻿using Discord;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Game.Attributes;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.ReadOnly;
using HeresonBot.DataClasses.Types;
using HeresonBot.Extensions.Numerics;
using HeresonBot.Extensions.Strings;
using HeresonBot.Settings;
using System.Collections.Generic;
using System.Text;

namespace HeresonStudioBot.Services.Discord
{
    /// <summary>
    /// Collection of static functions that help the message embedding process.
    /// </summary>
    static class EmbedBuilderService
    {
        /// <summary>
        /// Shows character stats.
        /// </summary>
        /// <param name="character">Character object.</param>
        /// <param name="imageUrl">ImageUrl string.</param>
        /// <param name="showStats">Shows stats.</param>
        /// <param name="showAchievements">Shows achievements.</param>
        /// <returns>Embedbuilder object.</returns>
        public static EmbedBuilder GetStatus(Character character, string imageUrl, bool showStats, bool showAchievements)
        {
            if (character == null)
                return null;

            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(Types.GetColorFromRank(character.Rank));

            builder.WithTitle($"**CHARACTER**");
            builder.WithDescription($"**Description:** {character.Description}");
            
            if (showStats)
                builder.AddField($"{Icons.ICON_STATS} **Stats** ``{character.Rank}-Rank``", GetAttributeStats(character.AttributeService?.AttributeList), true);

            if (showAchievements)
                builder.AddField($"{Icons.ICON_ACHIEVEMENTS} **Achievements** ", GetAchievements(character.AchievementService?.AchievementReferenceList), true);

            //if (showStats && showAchievements)
            //    builder.WithFooter($"Character created: {character.DateCreated}.");

            AddAuthorToBuilder(builder, character.Name, imageUrl);
            return builder;
        }

        /// <summary>
        /// Shows character equipment.
        /// </summary>
        /// <param name="character">Character object.</param>
        /// <param name="imageUrl">ImageUrl string.</param>
        /// <param name="showStats">Shows stats.</param>
        /// <param name="showDescription">Shows description.</param>
        /// <returns>Embedbuilder object.</returns>
        public static EmbedBuilder GetEquipment(Character character, string imageUrl, bool showStats, bool showDescription)
        {
            if (character == null)
                return null;

            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(Types.GetColorFromRank(character.Rank));
            builder.WithTitle($"**EQUIPMENT**");

            builder.WithDescription($"Your equipped items can be found here.");

            Equipment MainHand = character.InventoryService.FindEquipmentReference("mainhand", true)?.Item as Equipment;
            Equipment OffHand = character.InventoryService.FindEquipmentReference("offhand", true)?.Item as Equipment;
            Equipment Head = character.InventoryService.FindEquipmentReference("head", true)?.Item as Equipment;
            Equipment Body = character.InventoryService.FindEquipmentReference("body", true)?.Item as Equipment;
            Equipment Legs = character.InventoryService.FindEquipmentReference("legs", true)?.Item as Equipment;
            Equipment Shoes = character.InventoryService.FindEquipmentReference("shoes", true)?.Item as Equipment;

            AddEquipmentToBuilder(builder, "MainHand", MainHand, showStats, showDescription);
            AddEquipmentToBuilder(builder, "OffHand", OffHand, showStats, showDescription);
            AddEquipmentToBuilder(builder, "Head", Head, showStats, showDescription);
            AddEquipmentToBuilder(builder, "Body", Body, showStats, showDescription);
            AddEquipmentToBuilder(builder, "Legs", Legs, showStats, showDescription);
            AddEquipmentToBuilder(builder, "Shoes", Shoes, showStats, showDescription);

            AddAuthorToBuilder(builder, character.Name, imageUrl);

            return builder;
        }

        /// <summary>
        /// Helperfunction that adds equipment fields.
        /// </summary>
        /// <param name="builder">EmbedBuilder object.</param>
        /// <param name="slotname">Slotname.</param>
        /// <param name="equipment">Equipment object.</param>
        /// <param name="showStats">Shows stats.</param>
        /// <param name="showDescription">Shows description.</param>
        private static void AddEquipmentToBuilder(EmbedBuilder builder, string slotname, Equipment equipment, bool showStats, bool showDescription)
        {
            if (equipment != null)
            {
                builder.AddField($"{equipment.Icon} {equipment.Slot} ``{equipment.ItemQuality}``", $"{equipment.Name}", true);

                if (showStats)
                {
                    builder.AddField($"{Icons.ICON_STATS} Stats ``{equipment.Rank}-Rank``", 
                        GetAttributeStats(equipment.AttributeService?.AttributeList), true);
                    builder.AddField($"{Icons.ICON_ENCHANTMENTS} Enchantments",
                        $"Element: **{equipment.Element}**", true);
                }

                if (showDescription)
                    builder.AddField($"\u200b", $"**Description:** {equipment.Description}");
            }
            else
            {
                if (showStats || showDescription)
                    builder.AddField($"{slotname}", "None", false);
                else
                    builder.AddField($"{slotname}", "None", true);
            }
        }

        /// <summary>
        /// Get achievements.
        /// </summary>
        /// <param name="achievementList">List of Achievement objects.</param>
        /// <returns>StringBuilder object.</returns>
        private static StringBuilder GetAchievements(List<AchievementReference> achievementList)
        {
            if (achievementList == null)
                return new StringBuilder("-");

            StringBuilder sb = new StringBuilder();
            foreach (AchievementReference achiref in achievementList)
                sb.AppendLine($"{achiref.Achievement.Name} ``({achiref.DateCreated.ToShortDateString()})``");
            if (sb.Length == 0)
                sb.AppendLine("-");
            return sb;
        }

        /// <summary>
        /// Get attribute stats.
        /// </summary>
        /// <param name="attributeList">List of Attribute objects.</param>
        /// <returns>StringBuilder object.</returns>
        private static StringBuilder GetAttributeStats(List<Attribute> attributeList)
        {
            if (attributeList == null)
                return new StringBuilder("-");

            StringBuilder sb = new StringBuilder();
            foreach (Attribute attribute in attributeList)
            {
                string percentage = "";
                if (attribute.Multiplier > 0.0)
                    percentage = $"``(+{attribute.Multiplier.ToPercentage()})``";
                sb.AppendLine($"{attribute.AttributeType.ToString()}: **{attribute.Value.ToString()}** {percentage}");
            }
            if (sb.Length == 0)
                sb.AppendLine("None");
            return sb;
        }

        /// <summary>
        /// Shows character inventory.
        /// </summary>
        /// <param name="character">Character object.</param>
        /// <param name="imageUrl">ImageUrl string.</param>
        /// <returns>Embedbuilder object.</returns>
        public static EmbedBuilder GetInventory(Character character, string imageUrl)
        {
            if (character == null)
                return null;

            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(Types.GetColorFromRank(character.Rank));
            builder.WithTitle($"**INVENTORY**");

            builder.WithDescription($"Your collected items can be found here.");

            foreach (ItemReference itemref in character.InventoryService.ItemReferenceList)
                builder.AddField(
                    $"**{itemref.Item.Name}** ``x{itemref.Quantity}``",
                    $"{itemref.Item.Description}");

            AddAuthorToBuilder(builder, character.Name, imageUrl);

            return builder;
        }


        /// <summary>
        /// Helperfunction that adds author.
        /// </summary>
        /// <param name="builder">EmbedBuilder object.</param>
        /// <param name="characterName">Character name.</param>
        /// <param name="imageUrl">Image Url string.</param>
        private static void AddAuthorToBuilder(EmbedBuilder builder, string characterName, string imageUrl = null)
        {
            if (!string.IsNullOrEmpty(imageUrl))
                builder.WithAuthor(characterName, imageUrl);
            else
                builder.WithAuthor(characterName);
        }

        /// <summary>
        /// Gets the embed builder for (server) error messages.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>Embdedbuilder object.</returns>
        public static EmbedBuilder GetErrorText(string errorMessage)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(ColorSettings.ErrorColor);
            builder.WithTitle("SERVER MESSAGE");
            builder.WithDescription(errorMessage);
            builder.WithFooter("Error");
            builder.WithCurrentTimestamp();
            return builder;
        }

        /// <summary>
        /// Gets the embed builder for (server) success messages.
        /// </summary>
        /// <param name="successMessage">The success message.</param>
        /// <returns>Embdedbuilder object.</returns>
        public static EmbedBuilder GetSuccessText(string successMessage)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(ColorSettings.SuccessColor);
            builder.WithTitle("SERVER MESSAGE");
            builder.WithDescription(successMessage);
            builder.WithFooter("Success");
            builder.WithCurrentTimestamp();
            return builder;
        }
        
        /// <summary>
        /// Gets the embed builder for default messages.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="message">Message.</param>
        /// <returns>Embdedbuilder object.</returns>
        public static EmbedBuilder GetText(string title, string message)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(ColorSettings.AdminColor);
            builder.WithTitle(title);
            builder.WithDescription(message);
            return builder;
        }

        /// <summary>
        /// Gets the embed builder for default messages.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="message">Message.</param>
        /// <param name="footer">Footer.</param>
        /// <returns>Embdedbuilder object.</returns>
        public static EmbedBuilder GetText(string title, string message, string footer)
        {
            EmbedBuilder builder = new EmbedBuilder();
            builder.WithColor(ColorSettings.AdminColor);
            builder.WithTitle(title);
            builder.WithDescription(message);
            builder.WithFooter(footer);
            return builder;
        }
    }
}