﻿using Discord;
using Discord.Commands;
using HeresonBot.DataClasses.Settings;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Database;
using System.Threading.Tasks;

namespace HeresonBot.Services.Discord
{
    class DiscordMessageService
    {
        /// <summary>
        /// TODO: Leaderboards / Serverannouncements.
        /// [News] Daily News / Weekly News / Weather announcements.
        /// [ServerAnnouncements] Maintennance in 2 hours... / Double-exp weekend?
        /// Send a message to a specific channel.
        /// </summary>
        /// <param name="Context">SocketCommandContext.</param>
        /// <param name="channelType">ChannelType.</param>
        /// <param name="message">Message.</param>
        public static async Task SendMessageToChannel(SocketCommandContext Context, TextChannelType channelType, string message)
        {
            TextChannelSetting channelService = LocalStorage.FindChannelService(Context.Guild.Id);
            if (channelService != null)
            {
                TextChannel textChannel = channelService.FindTextChannel(channelType);
                if (textChannel != null)
                {
                    var channel = Context.Client.GetChannel(textChannel.TextChannelId) as IMessageChannel;

                    if (channel != null)
                        await channel.SendMessageAsync(message);
                }
            }
        }
    }
}