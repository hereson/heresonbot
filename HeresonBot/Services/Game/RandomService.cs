﻿using System;

namespace HeresonBot.Services.Game
{
    static class RandomService
    {
        private static Random _random = new Random();

        /// <summary>
        /// Gets random number between min and max.
        /// </summary>
        /// <param name="min">Min value.</param>
        /// <param name="max">Max value (exclusive).</param>
        /// <returns>Random Integer.</returns>
        public static int GetNumber(int min, int max)
        {
            return _random.Next(min, max);
        }

        /// <summary>
        /// Gets random number between 0 and max.
        /// </summary>
        /// <param name="max">Max value (exclusive).</param>
        /// <returns>Random Integer.</returns>
        public static int GetNumber(int max)
        {
            return _random.Next(max);
        }
    }
}