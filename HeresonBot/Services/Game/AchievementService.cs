﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Game.Units;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.Services.Game
{
    class AchievementService
    {
        [Key]
        public long AchievementServiceId { get; set; }

        public virtual Character Character { get; set; }
        public long UnitId { get; set; }
        
        /// <summary>
        /// List of Achievements.
        /// </summary>
        public virtual List<AchievementReference> AchievementReferenceList { get; set; } 

        public AchievementService() { }

        public AchievementService(Character character)
        {
            Character = character;
            AchievementReferenceList = new List<AchievementReference>();
        }

        /// <summary>
        /// Finds AchievementReference in the list.
        /// </summary>
        /// <param name="type">Name.</param>
        /// <returns>AchievementReference object.</returns>
        public AchievementReference FindAchievementReference(string name)
        {
            name = name.ToLower();
            foreach (AchievementReference achiref in AchievementReferenceList)
                if (achiref.Achievement.GetLowerName() == name)
                    return achiref;
            return null;
        }

        /// <summary>
        /// Adds AchievementReference to the list if it doesn't exist.
        /// </summary>
        /// <param name="type">Achievement object.</param>
        /// <returns>Boolean.</returns>
        public bool AddAchievementReference(Achievement achievement)
        {
            if (FindAchievementReference(achievement.Name) != null)
                return false;

            AchievementReferenceList.Add(new AchievementReference(this, achievement));
            return true;
        }

        /// <summary>
        /// Removes achievement from the list if it exists.
        /// </summary>
        /// <param name="type">Achievement object.</param>
        /// <returns>Boolean.</returns>
        public bool RemoveAchievement(string name)
        {
            AchievementReference achievement = FindAchievementReference(name);
            if (achievement == null)
                return false;

            return AchievementReferenceList.Remove(achievement);
        }
    }
}