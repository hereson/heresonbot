﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Attributes;
using HeresonBot.DataClasses.Game.Items;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Types;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.Services.Game
{
    class AttributeService
    {
        [Key]
        public long AttributeServiceId { get; set; }

        public virtual Equipment Equipment { get; set; }
        public long? ItemId { get; set; }

        public virtual Unit Unit { get; set; }
        public long? UnitId { get; set; }

        public virtual ItemReference ItemReference { get; set; }
        public long? ItemReferenceId { get; set; }

        /// <summary>
        /// List of Attributes.
        /// </summary>
        public virtual List<Attribute> AttributeList { get; set; }

        public AttributeService() { }

        public AttributeService(ItemReference itemref)
        {
            ItemReference = itemref;
            AttributeList = new List<Attribute>();
        }

        public AttributeService(Unit unit)
        {
            Unit = unit;
            AttributeList = new List<Attribute>();
        }

        public AttributeService(Equipment equipment)
        {
            Equipment = equipment;
            AttributeList = new List<Attribute>();
        }

        /// <summary>
        /// Finds attribute in the list.
        /// </summary>
        /// <param name="type">AttributeType enum.</param>
        /// <returns>Attribute object.</returns>
        public Attribute FindAttribute(AttributeType type)
        {
            foreach (Attribute attr in AttributeList)
                if (attr.AttributeType == type)
                    return attr;
            return null;
        }

        /// <summary>
        /// Adds new attribute to the list.
        /// </summary>
        /// <param name="type">AttributeType enum.</param>
        /// <param name="value">Value.</param>
        /// <param name="multiplier">Multiplier.</param>
        /// <returns>Boolean.</returns>
        public bool AddAttribute(AttributeType type, int value, double multiplier = 0)
        {
            Attribute attr = FindAttribute(type);
            if (attr == null)
            {
                AttributeList.Add(new Attribute(this, type, value, multiplier));
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds value to a specific attribute in the list.
        /// Returns the rolled dice value if succeeded.
        /// </summary>
        /// <param name="type">AttributeType enum.</param>
        /// <returns>Integer.</returns>
        public int? AddValueToAttribute(AttributeType type, Dice dice)
        {
            Attribute attr = FindAttribute(type);
            if (attr != null)
            {
                Attribute baseAttribute = null;
                if (Equipment != null)
                    baseAttribute = Equipment.AttributeService.FindAttribute(type);
                else if (ItemReference != null)
                    baseAttribute = ItemReference.AttributeService.FindAttribute(type);
                else
                    baseAttribute = Unit.AttributeService.FindAttribute(type);

                return attr.AddValue(baseAttribute, dice);
            }
            return null;
        }
    }
}