﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Dungeons;
using System.Collections.Generic;

namespace HeresonBot.Services.Game
{
    class DungeonGeneratorService
    {
        public Room RoomStart = null;

        public DungeonGeneratorService()
        {
            InitDungeon();
        }

        public Room InitDungeon()
        {
            Room roomStart = RoomStart;
            int startNumber = RandomService.GetNumber(4);
            for (int i = 0; i < startNumber; i++)
                RoomStart.AddRoom(new EmptyRoom());
            return GenerateDungeon(RoomStart);
        }

        public Room GenerateDungeon(Room room)
        {
            foreach (Room r in room.RoomList)
            {
                int nr = RandomService.GetNumber(4);
                r.AddRoom(GetRandomRoom());
                GenerateDungeon(r);
            }
            return room;
        }

        public Room GetRandomRoom()
        {
            Room room = null;
            for (int j = 0; j < 8; j++)
            {
                int rand = RandomService.GetNumber(10);
                if (rand < 2)
                    room = new EmptyRoom();
                else if (rand > 8)
                    room = new LeverRoom("Lever Room", "Find the right lever to open the path to the next room.");
            }
            return room;
        }
    }
}