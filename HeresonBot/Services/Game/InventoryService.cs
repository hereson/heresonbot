﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Items;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.Services.Game
{
    class InventoryService
    {
        [Key]
        public long InventoryServiceId { get; set; }

        public virtual Character Character { get; set; }
        public long UnitId { get; set; }

        /// <summary>
        /// List of Item References.
        /// </summary>
        public virtual List<ItemReference> ItemReferenceList { get; set; }
        
        public InventoryService() { }

        public InventoryService(Character character)
        {
            Character = character;
            ItemReferenceList = new List<ItemReference>();
        }

        /// <summary>
        /// Find ItemReference based on string.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <returns>ItemReference object.</returns>
        public ItemReference FindItemReference(string name)
        {
            name = name.ToLower();
            foreach (ItemReference itemRef in ItemReferenceList)
                if (itemRef.Item.GetLowerName() == name)
                    return itemRef;
            return null;
        }

        /// <summary>
        /// Find Equipment based on string.
        /// If isEquipped is true  - It will only search every equipment in your inventory that is equipped.
        /// If isEquipped is false - It will only search every equipment in your inventory that is not equipped.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="isEquipped">Boolean.</param>
        /// <returns>ItemReference object.</returns>
        public ItemReference FindEquipmentReference(string name, bool isEquipped)
        {
            name = name.ToLower();
            foreach (ItemReference itemRef in ItemReferenceList)
            {
                if (itemRef.IsEquipped == isEquipped)
                {
                    Equipment equipment = itemRef.Item as Equipment;
                    if (equipment != null)
                        if (equipment.GetLowerName() == name || equipment.Slot.ToString().ToLower() == name)
                            return itemRef;
                }
            }
            return null;
        }

        /// <summary>
        /// Adds Item to the list.
        /// </summary>
        /// <param name="item">Item object.</param>
        /// <param name="quantity">Quantity.</param>
        /// <returns>Boolean.</returns>
        public bool AddItemToInventory(Item item, int quantity = 1)
        {
            if (ItemReferenceList.Count > 32)
                return false;

            ItemReference itemRef = FindItemReference(item.Name);
            
            if (itemRef == null)
                ItemReferenceList.Add(new ItemReference(this, item, quantity, false));
            else
                itemRef.Quantity += quantity;
            return true;
        }

        /// <summary>
        /// Removes ItemReference from the list.
        /// </summary>
        /// <param name="name">String.</param>
        /// <param name="quantity">Quantity.</param>
        /// <returns>Boolean.</returns>
        public ItemReference RemoveItemFromInventory(string name, int quantity)
        {
            ItemReference itemReference = FindItemReference(name);
            if (itemReference != null)
            {
                if (itemReference.Quantity >= quantity)
                {
                    itemReference.Quantity -= quantity;
                    return itemReference;
                }
                else
                    return null;
            }
            return null;
        }

        /// <summary>
        /// Equips equipment.
        /// </summary>
        /// <param name="name">String.</param>
        /// <returns>ItemReference object.</returns>
        public ItemReference Equip(string name)
        {
            // Finds equipment that want to equip.
            ItemReference itemReference = FindEquipmentReference(name, false); 
            if (itemReference == null)
                return null;

            // Finds equipment that needs to be unequipped (if any)
            ItemReference currentItemReference = FindEquipmentReference((itemReference.Item as Equipment)?.Slot.ToString(), true);
            if (currentItemReference != null)
                currentItemReference.IsEquipped = false;

            itemReference.IsEquipped = true;
            return itemReference;
        }

        /// <summary>
        /// Unequips equipment.
        /// </summary>
        /// <param name="name">String.</param>
        /// <returns>ItemReference object.</returns>
        public ItemReference Unequip(string name)
        {
            // Finds equipment that want to equip.
            ItemReference itemReference = FindEquipmentReference(name, true);
            if (itemReference == null)
                return null;

            itemReference.IsEquipped = false;
            return itemReference;
        }
    }
}