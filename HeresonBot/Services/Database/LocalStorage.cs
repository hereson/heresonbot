﻿using HeresonBot.DataClasses.Settings;
using HeresonBot.DataClasses.Users;
using System.Collections.Concurrent;

namespace HeresonBot.Services.Database
{
    partial class LocalStorage
    {
        public static ConcurrentDictionary<ulong, TextChannelSetting> ChannelServices = new ConcurrentDictionary<ulong, TextChannelSetting>();
        public static ConcurrentDictionary<ulong, PartyUser> PartyUsers = new ConcurrentDictionary<ulong, PartyUser>();

        #region PartyUsers
        /// <summary>
        /// Find PartyUser in local dictionary.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <returns>PartyUser object.</returns>
        public static PartyUser FindUser(ulong id)
        {
            PartyUsers.TryGetValue(id, out PartyUser user);
            return user;
        }

        /// <summary>
        /// Check if PartyUser exists in local dictionary.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <returns>Boolean.</returns>
        public static bool ContainsUserId(ulong id)
        {
            return PartyUsers.ContainsKey(id);
        }

        /// <summary>
        /// Get PartyUser from local dictionary.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <returns>User object.</returns>
        public static PartyUser GetUser(ulong id)
        {
            return PartyUsers[id];
        }

        /// <summary>
        /// Add PartyUser to local dictionary.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <param name="user">PartyUser object.</param>
        public static void AddUser(ulong id, PartyUser user)
        {
            PartyUsers.TryAdd(id, user);
        }

        /// <summary>
        /// Remove PartyUser from local dictionary.
        /// </summary>
        /// <param name="id">Discord Id.</param>
        /// <returns>Boolean.</returns>
        public static bool RemoveUser(ulong id)
        {
            if (FindUser(id) != null)
                return PartyUsers.TryRemove(id, out _);
            return false;
        }

        #endregion

        #region ChannelServices
        /// <summary>
        /// Find ChannelService in local dictionary.
        /// </summary>
        /// <param name="guildId">Guild Id.</param>
        /// <returns>User object.</returns>
        public static TextChannelSetting FindChannelService(ulong guildId)
        {
            ChannelServices.TryGetValue(guildId, out TextChannelSetting ctcs);
            return ctcs;
        }

        /// <summary>
        /// Check if ChannelService exists in local dictionary.
        /// </summary>
        /// <param name="guildId">Guild Id.</param>
        /// <returns>Boolean.</returns>
        public static bool ContainsChannelService(ulong guildId)
        {
            return ChannelServices.ContainsKey(guildId);
        }

        /// <summary>
        /// Add ChannelService to local dictionary.
        /// </summary>
        /// <param name="guildId">Guild Id.</param>
        /// <param name="channelService">ChannelService object.</param>
        public static void AddChannelService(ulong guildId, TextChannelSetting channelService)
        {
            ChannelServices.TryAdd(guildId, channelService);
        }

        /// <summary>
        /// Remove ChannelService from local dictionary.
        /// </summary>
        /// <param name="guildId">Guild Id.</param>
        /// <returns>Boolean.</returns>
        public static bool RemoveChannelService(ulong guildId)
        {
            if (FindChannelService(guildId) != null)
                return ChannelServices.TryRemove(guildId, out _);
            return false;
        }
        #endregion
    }
}