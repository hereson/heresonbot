﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Users;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Game.Achievements;
using System.Threading.Tasks;
using HeresonBot.Extensions.Numerics;

namespace HeresonBot.Services.Database
{
    static class HeresonDbService
    {
        public static async Task<User> GetUserAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Users.FindAsync(id);
        }

        public static async Task<Character> GetCharacterAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Character> GetCharacterInventoryAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.InventoryService)
                .ThenInclude(inventory => inventory.ItemReferenceList)
                .ThenInclude(itemreference => itemreference.Item)
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Character> GetCharacterStatsAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.AttributeService)
                    .ThenInclude(service => service.AttributeList)
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Character> GetCharacterAchievementsAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.AchievementService)
                    .ThenInclude(achievements => achievements.AchievementReferenceList)
                    .ThenInclude(achiref => achiref.Achievement)
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Character> GetCharacterStatsAndAchievementsAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.AttributeService)
                    .ThenInclude(service => service.AttributeList)
                .Include(character => character.AchievementService)
                    .ThenInclude(achievements => achievements.AchievementReferenceList)
                    .ThenInclude(achiref => achiref.Achievement)
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Character> GetCharacterEquipmentAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.InventoryService)
                .ThenInclude(inventory => inventory.ItemReferenceList)
                .ThenInclude(itemreference => (itemreference.Item as Equipment))
                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Item> GetItemAsync(HeresonDbContext context, string itemName)
        {
            itemName = itemName.ToLower();
            return await context.Items
                .Where(item => item.GetLowerName() == itemName)
                .FirstOrDefaultAsync();
        }

        public static async Task<Character> GetCharacterEquipmentAndAttributesAsync(HeresonDbContext context, ulong discordId)
        {
            long id = discordId.ToLong();
            return await context.Characters
                .Include(character => character.InventoryService)
                    .ThenInclude(inventory => inventory.ItemReferenceList)
                        .ThenInclude(itemreference => itemreference.Item)
                        .ThenInclude(item => (item as Equipment).AttributeService)
                        .ThenInclude(service => service.AttributeList)

                .Include(character => character.InventoryService)
                    .ThenInclude(inventory => inventory.ItemReferenceList)
                        .ThenInclude(itemreference => itemreference.AttributeService)
                        .ThenInclude(service => service.AttributeList)

                .FirstOrDefaultAsync(character => character.__discordId == id);
        }

        public static async Task<Achievement> GetAchievementAsync(HeresonDbContext context, string itemName)
        {
            itemName = itemName.ToLower();
            return await context.Achievements
                .Where(achievement => achievement.GetLowerName() == itemName)
                .FirstOrDefaultAsync();
        }
    }
}