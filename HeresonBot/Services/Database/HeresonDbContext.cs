﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Game.Attributes;
using HeresonBot.DataClasses.Game.Items;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Settings;
using HeresonBot.DataClasses.Types;
using HeresonBot.DataClasses.Users;
using HeresonBot.Services.Game;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Configuration;

namespace HeresonBot.Services.Database
{
    class HeresonDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Unit> Units { get; set; }
        public DbSet<Character> Characters { get; set; }

        public DbSet<InventoryService> InventoryService { get; set; }
        public DbSet<ItemReference> ItemReferences { get; set; }
        
        public DbSet<Item> Items { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<Dice> Dice { get; set; }

        public DbSet<AttributeService> AttributeService { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
        
        public DbSet<AchievementService> AchievementService { get; set; }
        public DbSet<AchievementReference> AchievementReferences { get; set; }
        public DbSet<Achievement> Achievements { get; set; }

        public DbSet<TextChannelSetting> TextChannelSettings { get; set; }
        public DbSet<TextChannel> TextChannels { get; set; }
        public DbSet<RankSetting> RankSettings { get; set; }

        /// <summary>
        /// Initialize.
        /// </summary>
        /// <param name="builder">DbContextOptionsBuilder object.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(ConfigurationManager.ConnectionStrings["HeresonDbContext"].ConnectionString);
            builder.EnableSensitiveDataLogging(true);
        }

        /// <summary>
        /// No Query Tracking.
        /// </summary>
        public void DisableTracking()
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        /// <summary>
        /// On Model Creation.
        /// </summary>
        /// <param name="builder">ModelBuilder object.</param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RankSetting>(entity =>
            {
                entity.HasKey(ranksetting => ranksetting.RankSettingId);
                
                entity.Property(ranksetting => ranksetting.RankSettingId)
                    .ValueGeneratedOnAdd();

                entity.Property(ranksetting => ranksetting.Rank)
                    .HasConversion(new EnumToStringConverter<Rank>());

                entity.Property(ranksetting => ranksetting.__roleId)
                    .HasColumnName("RoleId")
                    .HasDefaultValue(0);
            });

            builder.Entity<TextChannel>(entity =>
            {
                entity.HasKey(tc => tc.__textChannelId);

                entity.Property(tc => tc.__textChannelId)
                    .HasColumnName("TextChannelId")
                    .ValueGeneratedNever();

                entity.Property(tc => tc.__roleId)
                    .HasColumnName("RoleId")
                    .HasDefaultValue(0);

                entity.Property(tc => tc.TextChannelType)
                    .HasConversion(new EnumToStringConverter<TextChannelType>());
            });

            builder.Entity<TextChannelSetting>(entity =>
            {
                entity.HasKey(tcs => tcs.__guildChannelId);

                entity.Property(tcs => tcs.__guildChannelId)
                    .HasColumnName("GuildChannelId")
                    .ValueGeneratedNever();
                
                entity.Property(tcs => tcs.__mainCategoryId)
                    .HasColumnName("MainCategoryId")
                    .ValueGeneratedNever();

                entity.Property(tcs => tcs.__gameCategoryId)
                    .HasColumnName("GameCategoryId")
                    .ValueGeneratedNever();

                entity.Property(tcs => tcs.__skillCategoryId)
                    .HasColumnName("SkillCategoryId")
                    .ValueGeneratedNever();
            });

            builder.Entity<User>(entity =>
            {
                entity.HasKey(user => user.__discordId);

                entity.Property(user => user.__discordId)
                    .HasColumnName("DiscordId")
                    .ValueGeneratedNever();

                entity.Property(user => user.Hectogons)
                    .HasDefaultValue(0);

                entity.Property(user => user.DateCreated)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("GETDATE()")
                    .ValueGeneratedOnAdd();

                //entity.Property(user => user.CurrentActiveCharacter); // Used to store which character is currently active.
                    
                entity.HasMany<Character>(user => user.CharacterList)
                    .WithOne(character => character.User)
                    .HasForeignKey(character => character.__discordId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Character>(entity =>
            {
                entity.HasBaseType<Unit>();

                entity.Property(character => character.__discordId)
                    .HasColumnName("DiscordId");

                entity.Property(character => character.DateCreated)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("GETDATE()")
                    .ValueGeneratedOnAdd();

                entity.HasOne<InventoryService>(character => character.InventoryService)
                    .WithOne(service => service.Character)
                    .HasForeignKey<InventoryService>(service => service.UnitId)
                    .OnDelete(DeleteBehavior.Cascade);
                
                entity.HasOne<AchievementService>(character => character.AchievementService)
                    .WithOne(service => service.Character)
                    .HasForeignKey<AchievementService>(service => service.UnitId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Unit>(entity =>
            {
                entity.HasKey(unit => unit.UnitId);
                
                entity.Property(unit => unit.UnitId)
                    .HasColumnName("UnitId")
                    .ValueGeneratedOnAdd();

                entity.Property(unit => unit.__discordId)
                    .HasColumnName("DiscordId");

                entity.Property(unit => unit.Name)
                    .HasMaxLength(32);

                entity.Property(unit => unit.Description)
                    .HasMaxLength(256);

                entity.Property(unit => unit.Rank)
                    .HasDefaultValue(Rank.F)
                    .HasConversion(new EnumToStringConverter<Rank>());

                entity.HasOne<AttributeService>(unit => unit.AttributeService) // AttributeService.
                    .WithOne(service => service.Unit)
                    .HasForeignKey<AttributeService>(service => service.UnitId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<AttributeService>(entity =>
            {
                entity.HasKey(service => service.AttributeServiceId);
                
                entity.Property(service => service.AttributeServiceId)
                    .ValueGeneratedOnAdd();

                entity.HasMany<Attribute>(service => service.AttributeList)
                    .WithOne(attribute => attribute.AttributeService)
                    .HasForeignKey(attribute => attribute.AttributeServiceId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne<Unit>(service => service.Unit)
                    .WithOne(unit => unit.AttributeService)
                    .HasForeignKey<AttributeService>(service => service.UnitId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne<Equipment>(service => service.Equipment)
                    .WithOne(equipment => equipment.AttributeService)
                    .HasForeignKey<AttributeService>(service => service.ItemId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne<ItemReference>(service => service.ItemReference)
                    .WithOne(itemref => itemref.AttributeService)
                    .HasForeignKey<AttributeService>(service => service.ItemReferenceId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Attribute>(entity =>
            {
                entity.HasKey(attribute => attribute.AttributeId);

                entity.Property(attribute => attribute.AttributeId)
                    .ValueGeneratedOnAdd();

                entity.Property(attribute => attribute.AttributeType)
                    .HasConversion(new EnumToStringConverter<AttributeType>());

                entity.Property(attribute => attribute.Value)
                    .HasDefaultValue(0);

                entity.Property(attribute => attribute.Multiplier)
                    .HasDefaultValue(0.0);
            });

            builder.Entity<AchievementService>(entity =>
            {
                entity.HasKey(service => service.AchievementServiceId);

                entity.Property(service => service.AchievementServiceId)
                    .ValueGeneratedOnAdd();

                entity.Property(service => service.UnitId)
                    .HasColumnName("UnitId");

                entity.HasMany<AchievementReference>(service => service.AchievementReferenceList)
                    .WithOne(achievement => achievement.AchievementService)
                    .HasForeignKey(achievement => achievement.AchievementServiceId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<AchievementReference>(entity =>
            {
                entity.HasKey(achiref => achiref.AchievementReferenceId);

                entity.Property(achiref => achiref.AchievementReferenceId)
                    .ValueGeneratedOnAdd();

                entity.Property(achiref => achiref.DateCreated)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("GETDATE()")
                    .ValueGeneratedOnAdd();

                entity.HasOne<Achievement>(achiref => achiref.Achievement)
                  .WithOne()
                  .OnDelete(DeleteBehavior.Restrict);
            } );

            builder.Entity<Achievement>(entity =>
            {
                entity.HasKey(achievement => achievement.AchievementId);

                entity.Property(achievement => achievement.AchievementId)
                    .ValueGeneratedOnAdd();
                
                entity.Property(achievement => achievement.Name)
                    .HasMaxLength(64);

                entity.Property(achievement => achievement.Description)
                    .HasMaxLength(256);
            });


            builder.Entity<InventoryService>(entity =>
            {
                entity.HasKey(service => service.InventoryServiceId);

                entity.Property(service => service.InventoryServiceId)
                    .ValueGeneratedOnAdd();

                entity.Property(service => service.UnitId)
                    .HasColumnName("UnitId");
            });

            builder.Entity<ItemReference>(entity =>
            {
                entity.HasKey(itemref => itemref.ItemReferenceId);

                entity.Property(itemref => itemref.ItemReferenceId)
                    .ValueGeneratedOnAdd();

                entity.Property(itemref => itemref.IsEquipped)
                    .HasDefaultValue(false);

                entity.Property(itemref => itemref.Quantity)
                    .HasDefaultValue(1);

                entity.HasOne<AttributeService>(itemref => itemref.AttributeService) // AttributeService.
                    .WithOne(service => service.ItemReference)
                    .HasForeignKey<AttributeService>(service => service.ItemReferenceId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne<InventoryService>(itemref => itemref.InventoryService) // InventoryService.
                    .WithMany(service => service.ItemReferenceList)
                    .HasForeignKey(itemref => itemref.InventoryServiceId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne<Item>(itemref => itemref.Item)
                    .WithOne()
                    .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Item>(entity =>
            {
                entity.HasKey(item => item.ItemId);

                entity.Property(item => item.ItemId)
                    .ValueGeneratedOnAdd();

                entity.Property(item => item.Name)
                    .HasMaxLength(64);

                entity.Property(item => item.Icon)
                    .HasMaxLength(64)
                    .HasDefaultValue("");

                entity.Property(item => item.Description)
                    .HasMaxLength(256);

                entity.Property(item => item.Price)
                    .HasDefaultValue(0);

                entity.Property(equipment => equipment.ItemQuality)
                    .HasConversion(new EnumToStringConverter<ItemQuality>());
            });

            builder.Entity<Equipment>(entity =>
            {
                entity.HasBaseType<Item>();

                entity.Property(equipment => equipment.Slot)
                    .HasConversion(new EnumToStringConverter<EquipmentSlot>());

                entity.Property(equipment => equipment.Element)
                    .HasConversion(new EnumToStringConverter<ElementType>());

                entity.Property(equipment => equipment.Rank)
                    .HasConversion(new EnumToStringConverter<Rank>());

                entity.HasOne<AttributeService>(equipment => equipment.AttributeService) // AttributeService.
                    .WithOne(service => service.Equipment)
                    .HasForeignKey<AttributeService>(service => service.ItemId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
            base.OnModelCreating(builder);
        }
    }
}