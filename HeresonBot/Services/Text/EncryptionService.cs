﻿using HeresonBot.DataClasses.Users;
using System.Text;

namespace HeresonBot.Services.Text
{
    class EncryptionService
    {
        public static string[] Keys = null;// FileReader.ReadFromAuthenticationFolder("passwords.token"); // TODO;

        /// <summary>
        /// Checks the input string of the player.
        /// </summary>
        /// <param name="index">Key index.</param>
        /// <param name="user">User object.</param>
        /// <param name="str">Input string.</param>
        /// <returns>Boolean.</returns>
        public static bool CheckString(int index, User user, string str)
        {
            return str == ComputeString(index, user);
        }

        /// <summary>
        /// Computes a string hash.
        /// </summary>
        /// <param name="index">Key index.</param>
        /// <param name="user">User object.</param>
        /// <returns>String.</returns>
        public static string ComputeString(int index, User user)
        {
            StringBuilder sb = new StringBuilder();
            string key = Keys[index];
            string userId = user.DiscordId.ToString();
            for (int i = 0; i < key.Length; i++)
            {
                if (i < userId.Length)
                {
                    char c = GetHashValue(key[i], userId[i].ToString());
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Gets a quick and dirty hashvalue using a custom rot function.
        /// TODO: Make sure that a user gets locked out if he/she tries to brute force this.
        /// </summary>
        /// <param name="value">Char.</param>
        /// <param name="rot">Rotation amount (0-9).</param>
        /// <returns>New char hash.</returns>
        private static char GetHashValue(char value, string rot)
        {
            int number = value;
            int rotValue = int.Parse(rot);
            if (number >= 'a' && number <= 'z')
                number += rotValue;
            else if (number >= 'A' && number <= 'Z')
                number -= rotValue;
            return (char)(number + 10);
        }
    }
}