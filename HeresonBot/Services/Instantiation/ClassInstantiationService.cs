﻿using System;

namespace HeresonBot.Services.Instantiation
{
    class ClassInstantiationService
    {
        /// <summary>
        /// Instantiates an existing class given an input string.
        /// </summary>
        /// <param name="className">Classname.</param>
        /// <param name="namespaceDirectory">The full namespace location of the class.</param>
        /// <param name="parameters">Optional parameters.</param>
        /// <returns>Class object.</returns>
        private static Object ConvertStringToClassObject(string className, string namespaceDirectory, params object[] parameters)
        {
            Object o = null;
            var type = Type.GetType(namespaceDirectory + "." + className);
            if (type != null)
                o = Activator.CreateInstance(type, parameters);
            else
                Console.WriteLine("Unable to create class <" + className + "> in the directory <" + namespaceDirectory + ">.");
            return o;
        }
    }
}