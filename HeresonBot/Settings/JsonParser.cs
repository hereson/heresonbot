﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeresonBot.Settings
{
    class JsonParser
    {
        /// <summary>
        /// Loads configuration from json-file. TODO: REWRITE THIS SO THAT ITS NOT FUCKING RECURSIVE!!!
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Configuration object.</returns>
        public static async Task<Configuration> LoadFromJson(string fileName)
        {
            if (File.Exists($"Settings/{fileName}"))
            {
                var json = "";
                using (var file = File.OpenRead($"Settings/{fileName}"))
                using (var sr = new StreamReader(file, new UTF8Encoding(false)))
                    json = await sr.ReadToEndAsync().ConfigureAwait(false);

                Configuration jsonconfig = JsonConvert.DeserializeObject<Configuration>(json);
                return jsonconfig;
            }
            else
            {
                Configuration jsonconfig = new Configuration();
                jsonconfig.CreateDefaultConfiguration("Settings", fileName);
                return await LoadFromJson(fileName).ConfigureAwait(false);
            }
        }
    }
}