﻿using Discord;

namespace HeresonBot.Settings
{
    class ColorSettings
    {
        public static Color AdminColor = Color.DarkGreen;
        public static Color Default = Color.LightOrange;
        public static Color ErrorColor = Color.Red;
        public static Color SuccessColor = Color.Green;
    }
}