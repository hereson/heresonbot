﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows.Forms;

namespace HeresonBot.Settings
{
    internal class Configuration
    {
        [JsonProperty("token")]
        public string Token { get; private set; } = @"<EMPTY>";

        [JsonProperty("prefix")]
        public string Prefix { get; private set; } = "-";

        public void CreateDefaultConfiguration(string directory, string path)
        {
            try
            {
                string json = JsonConvert.SerializeObject(this, Formatting.Indented);
                Directory.CreateDirectory(directory);
                File.WriteAllText($"{directory}/{path}", json);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}