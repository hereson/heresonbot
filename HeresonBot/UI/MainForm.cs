﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Game.Items;
using HeresonBot.DataClasses.Game.Items.DiceObjects;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Types;
using HeresonBot.DataClasses.Users;
using HeresonBot.Extensions.Numerics;
using HeresonBot.Services.Database;
using HeresonBot.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Color = System.Drawing.Color;

namespace HeresonBot
{
    public partial class MainForm : Form
    {
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;
        private Configuration _config;
        private int _argumentPos = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //DungeonGeneratorService dgs = new DungeonGeneratorService();
            //
        }


        #region Database
        /// <summary>
        /// Check if database is connected.
        /// </summary>
        /// <returns>Boolean.</returns>
        private async Task<bool> IsDatabaseConnected()
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                try
                {
                    await context.Database.EnsureCreatedAsync();
                }
                catch (Exception e)
                {
                    AppendLogText(e.Message.ToString());
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region DiscordBot
        /// <summary>
        /// Initialize the bot with the current configuration settings.
        /// </summary>
        private async void btn_Connect_Click(object sender, EventArgs e)
        {
            EnableConnectButton(false);
            if (!await IsDatabaseConnected())
            {
                EnableConnectButton(true);
            }
            else
            {
                using (HeresonDbContext context = new HeresonDbContext())
                {
                    Equipment equipment1 = new Equipment("Sword1", "SwordDescription1", 1, ItemQuality.Rare, EquipmentSlot.MainHand, ElementType.Flame, ":crossed_swords:");
                    equipment1.AttributeService.AddAttribute(AttributeType.Critical, 111, 1.0);
                    equipment1.AttributeService.AddAttribute(AttributeType.Damage, 3, 0.0);

                    Equipment equipment2 = new Equipment("Sword2", "SwordDescription2", 1, ItemQuality.Common, EquipmentSlot.MainHand, ElementType.None, "");
                    equipment2.AttributeService.AddAttribute(AttributeType.HP, 222, 2.0);

                    Achievement achi1 = new Achievement("Achi1", "HUGE DESCRIPTION!! OMFG.");
                    Achievement achi2 = new Achievement("Achi2", "HUGE DESCRIPTION222!! OMFG.");
                    Achievement achi3 = new Achievement("Achi3", "small descriptoin..meh.");

                    Dice itemNormalDice = new NormalDice();
                    Dice itemElementDice = new ElementDice();

                    if (!context.Equipment.Any(x => x.Name == "Sword2"))
                    {
                        context.Equipment.AddRange(equipment1, equipment2);
                        context.Achievements.AddRange(achi1, achi2, achi3);
                        context.Dice.AddRange(itemNormalDice, itemElementDice);
                        await context.SaveChangesAsync();
                    }
                }

                AppendLogText($"Loading settings...");
                _config = await JsonParser.LoadFromJson("config.json");
                AppendLogText($"Loaded settings.");
                await RunBotAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Disconnects bot.
        /// </summary>
        private async void btn_Disconnect_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(() =>
            {
                EnableConnectButton(true);
                EnableDisconnectButton(false);

                Invoke((Action)delegate
                {
                    cb_Guilds.Items.Clear();
                    cb_Textchannels.Items.Clear();
                    cb_Users.Items.Clear();
                });
            }));
            thread.Start();
            await _client.StopAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Run bot.
        /// </summary>
        public async Task RunBotAsync()
        {
            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                // Requires Discord.Net.Providers.WS4Net if you're on Windows 7.
                // Install-Package Discord.Net.Providers.WS4Net -Version 1.0.2
                WebSocketProvider = Discord.Net.Providers.WS4Net.WS4NetProvider.Instance,
                LogLevel = LogSeverity.Verbose
            });

            _commands = new CommandService();
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            await RegisterCommandsAsync();

            _client.Log += MessageLog;

            var clientReady = new TaskCompletionSource<bool>();

            Task SetClientReady()
            {
                var _ = Task.Run(async () =>
                {
                    clientReady.TrySetResult(true);
                    try
                    {
                        foreach (var chan in (await _client.GetDMChannelsAsync().ConfigureAwait(false)))
                            await chan.CloseAsync().ConfigureAwait(false);
                    }
                    catch { }
                    finally
                    { }
                });
                return Task.CompletedTask;
            }

            try
            {
                await _client.LoginAsync(TokenType.Bot, _config.Token).ConfigureAwait(false);
                await _client.StartAsync().ConfigureAwait(false);
                _client.Ready += SetClientReady;
                //await _client.SetGameAsync("Anime", null, ActivityType.Watching).ConfigureAwait(false);
                await _client.SetStatusAsync(UserStatus.Invisible).ConfigureAwait(false);
                await clientReady.Task.ConfigureAwait(false);
                _client.Ready -= SetClientReady;

                _client.JoinedGuild += Client_JoinedGuild;
                _client.LeftGuild += Client_LeftGuild;
                
                //foreach (var guild in _client.Guilds) // TODO ?
                //    cb_Guilds.Items.Add(guild.Name);

                using (HeresonDbContext context = new HeresonDbContext())
                {
                    foreach (var guild in _client.Guilds)
                    {
                        long longGuildId = guild.Id.ToLong();
                        var dbChannelService = context.TextChannelSettings
                             .Where(c => c.__guildChannelId == longGuildId)
                             .Include(t => t.TextChannelList)
                             .Include(r => r.RankSettingList)
                             .FirstOrDefault();
                        if (dbChannelService != null)
                            LocalStorage.AddChannelService(guild.Id, dbChannelService);
                    }
                }
                EnableDisconnectButton(true);
            }
            catch (Exception e)
            {
                AppendLogText(e.Message.ToString());
                EnableConnectButton(true);
                EnableDisconnectButton(false);
                return;
            }
            await Task.Delay(-1).ConfigureAwait(false);
        }

        private Task Client_LeftGuild(SocketGuild arg)
        {
            AppendLogText($"{arg?.Name} left the server.");
            return Task.CompletedTask;
        }

        private Task Client_JoinedGuild(SocketGuild arg)
        {
            AppendLogText($"{arg?.Name} joined server!");

            // Do Stuff here that updates the database with the new user.
            //var _ = Task.Run(async () =>
            //{

            //});
            return Task.CompletedTask;
        }

        /// <summary>
        /// Show guilds, textchannels and users.
        /// </summary>
        private void cb_Guilds_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_Textchannels.Items.Clear();
            cb_Users.Items.Clear();

            foreach (var guild in _client.Guilds)
            {
                if (guild.Name == cb_Guilds.Text)
                {
                    foreach (var tc in guild.TextChannels)
                        cb_Textchannels.Items.Add(tc.Name);

                    foreach (var user in guild.Users)
                        cb_Users.Items.Add(user.Username);
                }
            }
        }

        /// <summary>
        /// Message Logs.
        /// </summary>
        private Task MessageLog(LogMessage msg)
        {
            AppendLogText(msg.ToString());
            return Task.CompletedTask;
        }

        /// <summary>
        /// String delegate.
        /// </summary>
        /// <param name="text">String.</param>
        delegate void StringArgReturningVoidDelegate(string text);

        /// <summary>
        /// This method appends text to a RichTextBox in a thread-safe manner.
        /// > If the calling thread is different from the thread that created the TextBox control, this method creates a  
        /// StringArgReturningVoidDelegate and calls itself asynchronously using the Invoke method.  
        /// > If the calling thread is the same as the thread that created  
        /// the TextBox control, the Text property is set directly. 
        /// </summary>
        /// <param name="text">String.</param>
        private void AppendLogText(string text)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (rtb_Log.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(AppendLogText);
                Invoke(d, new object[] { text });
            }
            else
                rtb_Log.AppendText(text + Environment.NewLine);
        }

        /// <summary>
        /// Boolean delegate.
        /// </summary>
        /// <param name="b">Boolean.</param>
        delegate void BoolArgReturningVoidDelegate(bool b);

        /// <summary>
        /// This method enables/disables the ConnectButton in a threadsafe manner.
        /// </summary>
        /// <param name="b">Boolean.</param>
        private void EnableConnectButton(bool b)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (btn_Connect.InvokeRequired)
            {
                BoolArgReturningVoidDelegate d = new BoolArgReturningVoidDelegate(EnableConnectButton);
                Invoke(d, new object[] { b });
            }
            else
                btn_Connect.Enabled = b;
        }

        /// <summary>
        /// This method enables/disables the DisconnectButton in a threadsafe manner.
        /// </summary>
        /// <param name="b">Boolean.</param>
        private void EnableDisconnectButton(bool b)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.  
            // If these threads are different, it returns true.  
            if (btn_Disconnect.InvokeRequired)
            {
                BoolArgReturningVoidDelegate d = new BoolArgReturningVoidDelegate(EnableDisconnectButton);
                Invoke(d, new object[] { b });
            }
            else
                btn_Disconnect.Enabled = b;
        }

        /// <summary>
        /// Registers the command modules.
        /// </summary>
        private async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleIncomingMessagesAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly()).ConfigureAwait(false);
        }

        /// <summary>
        /// Handles all incoming messages.
        /// </summary>
        private async Task HandleIncomingMessagesAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;

            if (message == null) return;
            if (message.Author.IsBot) return;

            // If our bot is mentioned / called.
            if (message.HasStringPrefix(_config.Prefix, ref _argumentPos) || message.HasMentionPrefix(_client.CurrentUser, ref _argumentPos))
            {
                var Context = new SocketCommandContext(_client, message);

                CreateUser(Context);

                var result = await _commands.ExecuteAsync(Context, _argumentPos, _services);

                if (!result.IsSuccess)
                    AppendLogText(result.ToString());
            }
        }

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="Context">SocketCommandContext.</param>
        private void CreateUser(SocketCommandContext Context)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                long longDiscordId = Context.User.Id.ToLong();
                if (context.Users.Any(x => x.__discordId == longDiscordId))   // Entity exists.
                    return;
                else
                {
                    User user = new User(Context.User.Id, Context.User.Username, 0);
                    context.Users.Add(user);
                    context.SaveChanges();
                }
            }
        }

        private void btn_Connect_OnMouseEnter(object sender, EventArgs e) { ((Button)sender).BackColor = Color.FromArgb(223, Color.Moccasin); }
        private void btn_Connect_OnMouseLeave(object sender, EventArgs e) { ((Button)sender).BackColor = Color.Moccasin; }
        #endregion

        #region WindowsButtons
        /// <summary>
        /// Maximize form.
        /// </summary>
        private void lbl_Maximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
                //(sender as Label).Text = "□";
            }
            else if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                //(sender as Label).Text = "❐";
            }
        }

        /// <summary>
        /// Minimize form.
        /// </summary>
        private void lbl_Minimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Close form.
        /// </summary>
        private void lbl_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lbl_Minimize_OnMouseEnter(object sender, EventArgs e) { ChangeBackColor_OnEnter(sender as Label, Color.FromArgb(96, Color.OrangeRed)); }
        private void lbl_Minimize_OnMouseLeave(object sender, EventArgs e) { ChangeBackColor_OnLeave(sender as Label); }

        private void lbl_Maximize_OnMouseEnter(object sender, EventArgs e) { ChangeBackColor_OnEnter(sender as Label, Color.FromArgb(96, Color.OrangeRed)); }
        private void lbl_Maximize_OnMouseLeave(object sender, EventArgs e) { ChangeBackColor_OnLeave(sender as Label); }

        private void lbl_Close_OnMouseEnter(object sender, EventArgs e) { ChangeBackColor_OnEnter(sender as Label, Color.FromArgb(196, Color.OrangeRed)); }
        private void lbl_Close_OnMouseLeave(object sender, EventArgs e) { ChangeBackColor_OnLeave(sender as Label); }

        /// <summary>
        /// Changes color on mouse enter.
        /// </summary>
        /// <param name="label">Label object.</param>
        private void ChangeBackColor_OnEnter(Label label, Color color) { label.BackColor = color; }

        /// <summary>
        /// Changes color on mouse leave.
        /// </summary>
        /// <param name="label">Label object.</param>
        private void ChangeBackColor_OnLeave(Label label) { label.BackColor = Color.Transparent; }
        #endregion

        #region FormMoving
        private bool _isMouseDown;
        private Point _prevLocation;

        private void pnl_Header_OnMouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
            _prevLocation = e.Location;
        }

        private void pnl_Header_OnMouseUp(object sender, MouseEventArgs e)
        {
            _isMouseDown = false;
        }

        private void pnl_Header_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                Location = new Point((Location.X - _prevLocation.X) + e.X,
                    (Location.Y - _prevLocation.Y) + e.Y);
                Update();
            }
        }
        #endregion

        #region Log
        /// <summary>
        /// Scroll to the bottom when the textbox is updated.
        /// </summary>
        private void rtb_Log_TextChanged(object sender, EventArgs e)
        {
            if (rtb_Log.Text.Length < 8192)
            {
                rtb_Log.SelectionStart = rtb_Log.Text.Length;
                rtb_Log.ScrollToCaret();
            }
            else
                rtb_Log.Clear();
        }
        #endregion
    }
}