﻿namespace HeresonBot
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pnl_Header = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Minimize = new System.Windows.Forms.Label();
            this.lbl_AppName = new System.Windows.Forms.Label();
            this.lbl_Close = new System.Windows.Forms.Label();
            this.lbl_Maximize = new System.Windows.Forms.Label();
            this.rtb_Log = new System.Windows.Forms.RichTextBox();
            this.cb_Guilds = new System.Windows.Forms.ComboBox();
            this.cb_Textchannels = new System.Windows.Forms.ComboBox();
            this.cb_Users = new System.Windows.Forms.ComboBox();
            this.btn_Disconnect = new HeresonBot.Extensions.Forms.RoundedButton();
            this.btn_Connect = new HeresonBot.Extensions.Forms.RoundedButton();
            this.pnl_Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_Header
            // 
            this.pnl_Header.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_Header.BackColor = System.Drawing.Color.DarkOrange;
            this.pnl_Header.Controls.Add(this.label1);
            this.pnl_Header.Controls.Add(this.lbl_Minimize);
            this.pnl_Header.Controls.Add(this.lbl_AppName);
            this.pnl_Header.Controls.Add(this.lbl_Close);
            this.pnl_Header.Controls.Add(this.lbl_Maximize);
            this.pnl_Header.Location = new System.Drawing.Point(0, 0);
            this.pnl_Header.Margin = new System.Windows.Forms.Padding(0);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(1280, 32);
            this.pnl_Header.TabIndex = 0;
            this.pnl_Header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_Header_OnMouseDown);
            this.pnl_Header.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_Header_OnMouseMove);
            this.pnl_Header.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_Header_OnMouseUp);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Image = global::HeresonBot.Properties.Resources.icon_16x16_white;
            this.label1.Location = new System.Drawing.Point(8, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 26);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Minimize
            // 
            this.lbl_Minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Minimize.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Minimize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_Minimize.Location = new System.Drawing.Point(1184, 0);
            this.lbl_Minimize.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_Minimize.Name = "lbl_Minimize";
            this.lbl_Minimize.Padding = new System.Windows.Forms.Padding(4, 0, 0, 12);
            this.lbl_Minimize.Size = new System.Drawing.Size(32, 32);
            this.lbl_Minimize.TabIndex = 1;
            this.lbl_Minimize.Text = "_";
            this.lbl_Minimize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Minimize.Click += new System.EventHandler(this.lbl_Minimize_Click);
            this.lbl_Minimize.MouseEnter += new System.EventHandler(this.lbl_Minimize_OnMouseEnter);
            this.lbl_Minimize.MouseLeave += new System.EventHandler(this.lbl_Minimize_OnMouseLeave);
            // 
            // lbl_AppName
            // 
            this.lbl_AppName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_AppName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_AppName.ForeColor = System.Drawing.Color.White;
            this.lbl_AppName.Location = new System.Drawing.Point(36, 4);
            this.lbl_AppName.Name = "lbl_AppName";
            this.lbl_AppName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbl_AppName.Size = new System.Drawing.Size(212, 24);
            this.lbl_AppName.TabIndex = 0;
            this.lbl_AppName.Text = "Hereson Bot Alpha UI v.0.1";
            this.lbl_AppName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_Close
            // 
            this.lbl_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Close.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Close.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Close.Location = new System.Drawing.Point(1248, 0);
            this.lbl_Close.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_Close.Name = "lbl_Close";
            this.lbl_Close.Padding = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.lbl_Close.Size = new System.Drawing.Size(32, 32);
            this.lbl_Close.TabIndex = 1;
            this.lbl_Close.Text = "X";
            this.lbl_Close.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Close.Click += new System.EventHandler(this.lbl_Close_Click);
            this.lbl_Close.MouseEnter += new System.EventHandler(this.lbl_Close_OnMouseEnter);
            this.lbl_Close.MouseLeave += new System.EventHandler(this.lbl_Close_OnMouseLeave);
            // 
            // lbl_Maximize
            // 
            this.lbl_Maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_Maximize.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Maximize.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Maximize.Location = new System.Drawing.Point(1216, 0);
            this.lbl_Maximize.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_Maximize.Name = "lbl_Maximize";
            this.lbl_Maximize.Padding = new System.Windows.Forms.Padding(4, 0, 0, 7);
            this.lbl_Maximize.Size = new System.Drawing.Size(32, 32);
            this.lbl_Maximize.TabIndex = 1;
            this.lbl_Maximize.Text = "□";
            this.lbl_Maximize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Maximize.Click += new System.EventHandler(this.lbl_Maximize_Click);
            this.lbl_Maximize.MouseEnter += new System.EventHandler(this.lbl_Maximize_OnMouseEnter);
            this.lbl_Maximize.MouseLeave += new System.EventHandler(this.lbl_Maximize_OnMouseLeave);
            // 
            // rtb_Log
            // 
            this.rtb_Log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_Log.BackColor = System.Drawing.Color.NavajoWhite;
            this.rtb_Log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_Log.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.rtb_Log.Location = new System.Drawing.Point(36, 48);
            this.rtb_Log.Name = "rtb_Log";
            this.rtb_Log.ReadOnly = true;
            this.rtb_Log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtb_Log.Size = new System.Drawing.Size(640, 520);
            this.rtb_Log.TabIndex = 2;
            this.rtb_Log.Text = "";
            this.rtb_Log.TextChanged += new System.EventHandler(this.rtb_Log_TextChanged);
            // 
            // cb_Guilds
            // 
            this.cb_Guilds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_Guilds.FormattingEnabled = true;
            this.cb_Guilds.Location = new System.Drawing.Point(1150, 149);
            this.cb_Guilds.Name = "cb_Guilds";
            this.cb_Guilds.Size = new System.Drawing.Size(121, 21);
            this.cb_Guilds.TabIndex = 6;
            this.cb_Guilds.SelectedIndexChanged += new System.EventHandler(this.cb_Guilds_SelectedIndexChanged);
            // 
            // cb_Textchannels
            // 
            this.cb_Textchannels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_Textchannels.FormattingEnabled = true;
            this.cb_Textchannels.Location = new System.Drawing.Point(1150, 243);
            this.cb_Textchannels.Name = "cb_Textchannels";
            this.cb_Textchannels.Size = new System.Drawing.Size(121, 21);
            this.cb_Textchannels.TabIndex = 7;
            // 
            // cb_Users
            // 
            this.cb_Users.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_Users.FormattingEnabled = true;
            this.cb_Users.Location = new System.Drawing.Point(1150, 194);
            this.cb_Users.Name = "cb_Users";
            this.cb_Users.Size = new System.Drawing.Size(121, 21);
            this.cb_Users.TabIndex = 8;
            // 
            // btn_Disconnect
            // 
            this.btn_Disconnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Disconnect.BackColor = System.Drawing.Color.Moccasin;
            this.btn_Disconnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Disconnect.Enabled = false;
            this.btn_Disconnect.FlatAppearance.BorderSize = 0;
            this.btn_Disconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Disconnect.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Disconnect.Image = global::HeresonBot.Properties.Resources.icon_16x16_connect;
            this.btn_Disconnect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Disconnect.Location = new System.Drawing.Point(1159, 93);
            this.btn_Disconnect.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Disconnect.Name = "btn_Disconnect";
            this.btn_Disconnect.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.btn_Disconnect.Size = new System.Drawing.Size(112, 32);
            this.btn_Disconnect.TabIndex = 5;
            this.btn_Disconnect.Text = "Disconnect";
            this.btn_Disconnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Disconnect.UseVisualStyleBackColor = false;
            this.btn_Disconnect.Click += new System.EventHandler(this.btn_Disconnect_Click);
            // 
            // btn_Connect
            // 
            this.btn_Connect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Connect.BackColor = System.Drawing.Color.Moccasin;
            this.btn_Connect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Connect.FlatAppearance.BorderSize = 0;
            this.btn_Connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Connect.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Connect.Image = global::HeresonBot.Properties.Resources.icon_16x16_connect;
            this.btn_Connect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Connect.Location = new System.Drawing.Point(1159, 48);
            this.btn_Connect.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.btn_Connect.Size = new System.Drawing.Size(112, 32);
            this.btn_Connect.TabIndex = 1;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Connect.UseVisualStyleBackColor = false;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            this.btn_Connect.MouseEnter += new System.EventHandler(this.btn_Connect_OnMouseEnter);
            this.btn_Connect.MouseLeave += new System.EventHandler(this.btn_Connect_OnMouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(1280, 640);
            this.Controls.Add(this.cb_Users);
            this.Controls.Add(this.cb_Textchannels);
            this.Controls.Add(this.cb_Guilds);
            this.Controls.Add(this.btn_Disconnect);
            this.Controls.Add(this.rtb_Log);
            this.Controls.Add(this.btn_Connect);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.pnl_Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_Header;
        private System.Windows.Forms.Label lbl_Minimize;
        private System.Windows.Forms.Label lbl_Close;
        private System.Windows.Forms.Label lbl_AppName;
        private System.Windows.Forms.Label lbl_Maximize;
        private System.Windows.Forms.Label label1;
        private Extensions.Forms.RoundedButton btn_Connect;
        private System.Windows.Forms.RichTextBox rtb_Log;
        private Extensions.Forms.RoundedButton btn_Disconnect;
        private System.Windows.Forms.ComboBox cb_Guilds;
        private System.Windows.Forms.ComboBox cb_Textchannels;
        private System.Windows.Forms.ComboBox cb_Users;
    }
}

