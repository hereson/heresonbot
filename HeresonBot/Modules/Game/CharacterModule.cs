﻿using Discord.Commands;
using Discord.WebSocket;
using HeresonBot.Services.Database;
using HeresonStudioBot.Services.Discord;
using System.Threading.Tasks;
using HeresonBot.DataClasses.Game.Units;
using Discord;
using HeresonBot.Extensions.Strings;

namespace HeresonBot.Modules
{
    public class CharacterModule : ModuleBase<SocketCommandContext>
    {
        /// <summary>
        /// Shows your character information.
        /// </summary>
        [Command("character")]
        [Summary("Shows character information: -character <params> <user>" +
            "\nAdditional params: [stats, achievements, all]")]
        public async Task ShowCharacterInformation(string args = "", SocketGuildUser otherUser = null)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                context.DisableTracking();

                ulong id = Context.User.Id;
                if (otherUser != null)
                    id = otherUser.Id;

                EmbedBuilder eb = null;
                Character character = null;

                switch (args)
                {
                    case "all":
                        character = await HeresonDbService.GetCharacterStatsAndAchievementsAsync(context, id);
                        eb = EmbedBuilderService.GetStatus(character, Context.Guild.GetUser(id).GetAvatarUrl(), true, true);
                        break;
                    case "stats":
                        character = await HeresonDbService.GetCharacterStatsAsync(context, id);
                        eb = EmbedBuilderService.GetStatus(character, Context.Guild.GetUser(id).GetAvatarUrl(), true, false);
                        break;
                    case "achievements":
                        character = await HeresonDbService.GetCharacterAchievementsAsync(context, id);
                        eb = EmbedBuilderService.GetStatus(character, Context.Guild.GetUser(id).GetAvatarUrl(), false, true);
                        break;
                    default:
                        character = await HeresonDbService.GetCharacterAsync(context, id);
                        eb = EmbedBuilderService.GetStatus(character, Context.Guild.GetUser(id).GetAvatarUrl(), false, false);
                        break;
                }
                if (character != null)
                    await ReplyAsync("", false, eb?.Build()).ConfigureAwait(false);
                else
                    await ReplyAsync(TextExtension.ERROR_CHARACTER_NOT_FOUND).ConfigureAwait(false);
            
            }
        }

        /// <summary>
        /// Changes your character name.
        /// </summary>
        [Command("changename")]
        [Summary("Changes your character name: -changename <new_character_name>")]
        public async Task RenameCharacter(string name)
        {
            if (string.IsNullOrEmpty(name) || name.Length > 32 || !name.ContainsAlphaNumericCharacters())
            {
                await ReplyAsync("Alphanumeric characters only, no spaces (max 32 characters).").ConfigureAwait(false);
                return;
            }

            using (HeresonDbContext context = new HeresonDbContext())
            {
                var character = await HeresonDbService.GetCharacterAsync(context, Context.User.Id);
                if (character != null)
                {
                    character.Name = name;
                    await context.SaveChangesAsync();

                    await ReplyAsync($"Changed your charactername to: {name}.").ConfigureAwait(false);
                }
                else
                {
                    await ReplyAsync(TextExtension.ERROR_CHARACTER_NOT_FOUND).ConfigureAwait(false);
                }
            }
        }

        /// <summary>
        /// Changes your character description.
        /// </summary>
        [Command("changedescription")]
        [Summary("Changes your character description: -changedescription <new_character_description>")]
        public async Task UpdateDescriptionCharacter([Remainder] string description)
        {
            if (string.IsNullOrEmpty(description) || description.Length > 256 || !description.ContainsAlphaNumericCharactersAndPunctuation())
            {
                await ReplyAsync("Alphanumeric characters and the following symbols: ( : \" ! .  ^ _ - ? ; ) are only allowed (max 256 characters).").ConfigureAwait(false);
                return;
            }
            using (HeresonDbContext context = new HeresonDbContext())
            {
                var character = await HeresonDbService.GetCharacterAsync(context, Context.User.Id);
                if (character != null)
                {
                    character.Description = description;
                    await context.SaveChangesAsync();

                    await ReplyAsync($"Changed your character description to:\n \"{description}\"").ConfigureAwait(false);
                }
                else
                {
                    await ReplyAsync(TextExtension.ERROR_CHARACTER_NOT_FOUND);
                }
            }
        }

        /// <summary>
        /// Changes your character gender. Used for "He/She" text-flavor output. I've disabled/removed it for now.
        /// </summary>
        //[Command("changecharactergender")]
        //[Alias("changechargender")]
        //[Summary("Changes your character gender.")]
        //public async Task UpdateGender(string gender)
        //{
        //    if (Enum.TryParse(gender, true, out Gender genderType))
        //    {
        //        using (HeresonDbContext context = new HeresonDbContext())
        //        {
        //            var character = await HeresonDbService.GetCharacterAsync(context, Context.User.Id);
        //            if (character != null)
        //            {
        //                character.Gender = genderType;
        //                await context.SaveChangesAsync();

        //                await ReplyAsync($"Updated character gender information.");
        //            }
        //            else
        //            {
        //                await ReplyAsync(TextService.ERROR_CHARACTER_NOT_FOUND);
        //            }
        //        }
        //    }
        //}
    }
}