﻿using Discord;
using Discord.Commands;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Database;
using HeresonBot.Extensions.Discord;
using HeresonStudioBot.Services.Discord;
using System;
using System.Threading.Tasks;

namespace HeresonBot.Modules
{
    [IsInTextChannel(TextChannelType.Character)]
    public class InventoryModule : ModuleBase<SocketCommandContext>
    {
        /// <summary>
        /// Shows all items in inventory.
        /// </summary>
        [Command("inventory")]
        [Alias("items")]
        [Summary("Shows all items in inventory: -inventory <params>" +
            "\nAdditional params: [stats, equipment, all]")]
        public async Task ShowInventory([Remainder] string args = "") // TODO: Params here
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                context.DisableTracking();
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                EmbedBuilder eb = EmbedBuilderService.GetInventory(character, Context.User.GetAvatarUrl());
                await ReplyAsync("", false, eb?.Build()).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Shows all equipped items.
        /// </summary>
        [Command("equipment")]
        [Summary("Shows all equipped items: -equipment <params>" +
            "\nAdditional params: [stats, description, all]")]
        public async Task ShowEquipment(string args = "")
        {
            args = args.ToLower();
            using (HeresonDbContext context = new HeresonDbContext())
            {
                context.DisableTracking();
                EmbedBuilder eb;
                Character character;
                switch (args)
                {
                    case "all":
                        character = await HeresonDbService.GetCharacterEquipmentAndAttributesAsync(context, Context.User.Id);
                        eb = EmbedBuilderService.GetEquipment(character, Context.User.GetAvatarUrl(), true, true);
                        break;
                    case "stats":
                        character = await HeresonDbService.GetCharacterEquipmentAndAttributesAsync(context, Context.User.Id);
                        eb = EmbedBuilderService.GetEquipment(character, Context.User.GetAvatarUrl(), true, false);
                        break;
                    case "description":
                        character = await HeresonDbService.GetCharacterEquipmentAsync(context, Context.User.Id);
                        eb = EmbedBuilderService.GetEquipment(character, Context.User.GetAvatarUrl(), false, true);
                        break;
                    default:
                        character = await HeresonDbService.GetCharacterEquipmentAsync(context, Context.User.Id);
                        eb = EmbedBuilderService.GetEquipment(character, Context.User.GetAvatarUrl(), false, false);
                        break;
                }
                await ReplyAsync("", false, eb?.Build()).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Use item from inventory.
        /// </summary>
        [Command("useitem")]
        [Summary("Use an item from inventory: -use <item_name>")]
        public async Task UseItemFromInventory([Remainder] string itemName)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                ItemReference itemRefToRemove = character.InventoryService.RemoveItemFromInventory(itemName, 1);
                if (itemRefToRemove != null)
                {
                    //itemRefToRemove.Item.Use(); // TODO: Use??
                    if (itemRefToRemove.Quantity == 0)
                        context.ItemReferences.Remove(itemRefToRemove);
                    await context.SaveChangesAsync();
                    await ReplyAsync($"Used **{itemName}**.").ConfigureAwait(false);
                }
                else
                    await ReplyAsync($"Unable to use **{itemName}**.").ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Removes item from inventory.
        /// </summary>
        [Command("removeitem")]
        [Summary("Removes an item from inventory: -remove <quantity> <item_name>")]
        public async Task RemoveItemFromInventory(int quantity, [Remainder] string itemName)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                ItemReference itemRefToRemove = character.InventoryService.RemoveItemFromInventory(itemName, quantity);
                if (itemRefToRemove != null)
                {
                    if (itemRefToRemove.Quantity == 0)
                        context.ItemReferences.Remove(itemRefToRemove);
                    await context.SaveChangesAsync();
                    await ReplyAsync($"Removed {quantity} **{itemName}**.").ConfigureAwait(false);
                }
                else
                    await ReplyAsync($"Unable to remove **{itemName}** (x{quantity}).").ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Equips Item.
        /// </summary>
        [Command("equip")]
        [Summary("Equips an item: -equip <item_name>")]
        public async Task Equip([Remainder] string itemName)
        {
            // Disable equipping 'MainHand', 'OffHand' enum. values.
            foreach (EquipmentSlot slot in Enum.GetValues(typeof(EquipmentSlot)))
                if (slot.ToString().ToLower() == itemName)
                    return;

            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                ItemReference itemRef = character.InventoryService.Equip(itemName);
                if (itemRef != null)
                {
                    await context.SaveChangesAsync();
                    await ReplyAsync($"Equipped **{itemRef.Item.Name}**.").ConfigureAwait(false);
                }
                else
                    await ReplyAsync($"Unable to equip **{itemName}**.").ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Unequips Item.
        /// </summary>
        [Command("unequip")]
        [Summary("Unequips an item: -unequip <item_name_or_equipmentslot_name>")]
        public async Task Unequip([Remainder] string itemNameOrSlotName)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                ItemReference itemRef = character.InventoryService.Unequip(itemNameOrSlotName);
                if (itemRef != null)
                {
                    await context.SaveChangesAsync();
                    await ReplyAsync($"Unequipped **{itemRef.Item.Name}**.").ConfigureAwait(false);
                }
                else
                    await ReplyAsync($"Unable to unequip **{itemNameOrSlotName}**.").ConfigureAwait(false);
            }
        }
    }
}