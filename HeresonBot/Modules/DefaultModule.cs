﻿using Discord.Commands;
using HeresonStudioBot.Services.Discord;
using System.Threading.Tasks;

namespace HeresonBot.Modules
{
    public class DefaultModule : ModuleBase<SocketCommandContext>
    {
        /// <summary>
        /// Ping command.
        /// </summary>
        [Command("ping")]
        [Summary("Retrieves your ping to the server.")]
        public async Task Ping()
        {
            await ReplyAsync($"Your ping to the server is: {Context.Client.Latency} ms.");
        }

        /// <summary>
        /// Help command.
        /// </summary>
        [Command("info")]
        [Summary("Shows the bot information.")]
        public async Task ShowInfo()
        {
            await ReplyAsync("", false, EmbedBuilderService.GetText("HeresonBot Alpha (v1.0)", "A C# DiscordBot written by Hereson#1731.").Build());
        }

        /// <summary>
        /// Help command.
        /// </summary>
        [Command("help")]
        [Summary("Shows the help information.")]
        public async Task ShowHelp()
        {
            string msg =
                "```\n[ GENERAL COMMANDS ]" +
                "\n**Please start by creating your own character with the -createcharacter command." +
                    "\n-help                             - Show available commands." +

                    "\n[ CHARACTER COMMANDS]" +
                    "\n-login                            - Login if you've already created a character." +
                    "\n-logout                           - Logout." +
                    "\n-createcharacter <character name> - Creates a character with the given name." +
                    "\n-renamecharacter <character name> - Renames your character with the given name." +
                    "\n-stats <@username>                - Display player stats." +
                    "\n-items <@username>                - Display player inventory." +
                    "\n-equipment <@username>            - Display player equipment." +
                    "\n-achievements <@username>         - Displays achievements." +
                    "\n-shop                             - Enters the shop" +
                    "```";
            //await ReplyAsync(msg);
        }
        
        //[Command("analysis")]
        //[Summary("Just Yorha things.")]
        //public async Task Analysis([Remainder] string message)
        //{
        //    if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
        //        return;

        //    await Context.Message.DeleteAsync();
        //    await ReplyAsync("Analysing...", false, EmbedBuilderService.GetText("ANALYSIS", message).Build());
        //}

        //[Command("proposal")]
        //[Summary("Just Yorha things.")]
        //public async Task Proposal([Remainder] string message)
        //{
        //    if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
        //        return;

        //    await Context.Message.DeleteAsync();
        //    await ReplyAsync("Proposal...", false, EmbedBuilderService.GetText("PROPOSAL", message).Build());
        //}
    }
}