﻿using Discord;
using Discord.Commands;
using Discord.Rest;
using HeresonBot.DataClasses.Settings;
using HeresonBot.DataClasses.Types;
using HeresonBot.Extensions.Discord.Channels;
using HeresonBot.Extensions.Discord.Preconditions;
using HeresonBot.Extensions.Numerics;
using HeresonBot.Services.Database;
using HeresonBot.Services.Discord;
using HeresonBot.Settings;
using HeresonStudioBot.Services.Discord;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeresonBot.Modules
{
    [IsHereson]
    public class AdminModule : ModuleBase<SocketCommandContext>
    {
        [Command("sendmessage")]
        [Summary("Sends message to a specific channel.")]
        public async Task SendMessageToChannel(string channelName, [Remainder] string message)
        {
            if (Enum.TryParse(channelName, true, out TextChannelType channelType))
                await DiscordMessageService.SendMessageToChannel(Context, channelType, message);
        }

        [Command("findgamechannels -y")]
        [Summary("Finds the game channels and respective categories.")]
        public async Task FindChannelsAsync()
        {
            if (LocalStorage.FindChannelService(Context.Guild.Id) != null)
            {
                await ReplyAsync("", false, EmbedBuilderService.GetErrorText("Database already contains existing settings.").Build());
                return;
            }

            // Find all roles.
            List<RankSetting> rankSettingList = new List<RankSetting>();
            foreach (Rank rank in Enum.GetValues(typeof(Rank)))
            {
                var role = Context.Guild.Roles.FirstOrDefault(r => r.Name == (rank.ToString() + "-Rank"));
                if (role != null)
                    rankSettingList.Add(new RankSetting(rank, role.Id));
            }

            var mainCategory = Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name == "Foxie's Tower of Dice");
            var gameCategory = Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name == "Game");
            var skillCategory = Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name == "Skills");

         
            using (HeresonDbContext context = new HeresonDbContext())
            {
                TextChannelSetting settings = new TextChannelSetting(Context.Guild.Id, mainCategory.Id, gameCategory.Id, skillCategory.Id,
                    rankSettingList);

                foreach (TextChannelType tc in Enum.GetValues(typeof(TextChannelType)))
                {
                    var textchannel = Context.Guild.Channels.FirstOrDefault(c => c.Name == tc.ToString().ToLower());
                    ulong roleId = 0;
                    var role = Context.Guild.Roles.FirstOrDefault(r => r.Name.ToLower() == textchannel.Name.ToLower());
                    if (role != null)
                        roleId = role.Id;
                    settings.AddTextChannel(tc, textchannel.Id, roleId);
                }

                LocalStorage.AddChannelService(Context.Guild.Id, settings);
                context.TextChannelSettings.Add(settings);
                context.SaveChanges();
            }
            await ReplyAsync("Found and saved all channels.");
        }

        /// <summary>
        /// Game Channels Creation.
        /// </summary>
        [Command("creategamechannels -y")]
        [Summary("Creates all game-related channels.")]
        public async Task CreateChannelsAsync()
        {
            if (LocalStorage.FindChannelService(Context.Guild.Id) != null)
            {
                await ReplyAsync("", false, EmbedBuilderService.GetErrorText("Unable to create game channels. Database already contains existing settings.").Build());
                return;
            }

            /// Role creation.
            List<RankSetting> rankSettingList = new List<RankSetting>();
            foreach(Rank rank in Enum.GetValues(typeof(Rank)))
            {
                RestRole role = await CreateRoleAsync(rank.ToString() + "-Rank", GuildPermissionsExt.ReadSendHistoryMention, Types.GetColorFromRank(rank), true);
                rankSettingList.Add(new RankSetting(rank, role.Id));
            }

            /// Category creation.
            var mainCategory = await CreateCategoryChannelAsync("Foxie's Tower of Dice");
            var gameCategory = await CreateCategoryChannelAsync("Game");
            var skillCategory = await CreateCategoryChannelAsync("Skills");
            
            /// Main - Read-Only TextChannels creation.
            var serverannouncements = await CreateReadOnlyChannelAsync(TextChannelType.ServerAnnouncements, mainCategory);
            await serverannouncements.ModifyAsync(x => x.Topic = "ServerAnnouncements.");
            var leaderboard = await CreateReadOnlyChannelAsync(TextChannelType.Leaderboard, mainCategory);
            await serverannouncements.ModifyAsync(x => x.Topic = "Leaderboards.");

            /// Game - Normal TextChannel creation.
            var town = await CreateTextChannelAsync(TextChannelType.Town, gameCategory);
            var townShop = await CreateTextChannelAsync(TextChannelType.TownShop, gameCategory);
            var character = await CreateTextChannelAsync(TextChannelType.Character, gameCategory);

            var merchant = await CreateRoleLockedTextChannelAsync(TextChannelType.Merchant, gameCategory);

            /// Gathering Skills - TextChannel creation.
            List<Tuple<TextChannelType, RestTextChannel, RestRole>> skills = new List<Tuple<TextChannelType, RestTextChannel, RestRole>>();
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Fishing, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Woodcutting, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Mining, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Herbalism, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Cooking, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Crafting, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Smithing, skillCategory));
            skills.Add(await CreateRoleLockedTextChannelAsync(TextChannelType.Alchemy, skillCategory));

            /// Database.
            using (HeresonDbContext context = new HeresonDbContext())
            {
                TextChannelSetting settings = new TextChannelSetting(Context.Guild.Id, mainCategory.Id, gameCategory.Id, skillCategory.Id,
                    rankSettingList);

                settings.AddTextChannel(TextChannelType.ServerAnnouncements, serverannouncements.Id, 0);
                settings.AddTextChannel(TextChannelType.Leaderboard, leaderboard.Id, 0);
                settings.AddTextChannel(TextChannelType.Town, town.Id, 0);
                settings.AddTextChannel(TextChannelType.TownShop, townShop.Id, 0);
                settings.AddTextChannel(TextChannelType.Character, character.Id,0);
                settings.AddTextChannel(merchant.Item1, merchant.Item2.Id, merchant.Item3.Id);
                
                foreach (var kvp in skills)
                    settings.AddTextChannel(kvp.Item1, kvp.Item2.Id, kvp.Item3.Id);

                LocalStorage.AddChannelService(Context.Guild.Id, settings);
                context.TextChannelSettings.Add(settings);
                context.SaveChanges();
            }
            
            StringBuilder sb = new StringBuilder();
            foreach (var kvp in skills)
                sb.Append($"{kvp.Item2.Mention}  ");

            EmbedBuilder eb = new EmbedBuilder();
            eb.WithColor(ColorSettings.AdminColor);
            eb.WithTitle("SERVER MESSAGE");
            eb.WithDescription("Created all roles and game-related channels for the server.");
            eb.AddField("Important Server Announcements:", serverannouncements.Mention);
            eb.AddField("Leaderboard:", leaderboard.Mention);
            eb.AddField("The town:", town.Mention);
            eb.AddField("The town's local shop:", townShop.Mention);
            eb.AddField("Character management:", character.Mention);
            eb.AddField("A mysterious travelling merchant may appear here one day:", merchant.Item2.Mention);
            eb.AddField("Added skill channels: ", sb);
            eb.WithFooter("Spaghetti Code! #Foxie");
            eb.WithCurrentTimestamp();
            await ReplyAsync("", false, eb.Build());
        }

        /// <summary>
        /// Game Channels Deletion.
        /// </summary>
        [Command("removegamechannels -y")]
        [Summary("Removes all game-related channels.")]
        public async Task RemoveChannelsAsync()
        {
            if (LocalStorage.FindChannelService(Context.Guild.Id) == null)
            {
                await ReplyAsync("", false, EmbedBuilderService.GetErrorText("Unable to remove game channels. Database does not contain any settings.").Build());
                return;
            }

            using (HeresonDbContext context = new HeresonDbContext())
            {
                var channelService = LocalStorage.FindChannelService(Context.Guild.Id);
                if (channelService != null)
                {
                    /// Discord TextChannel deletion.
                    foreach (var textchannel in channelService.TextChannelList)                      // Delete textchannels.
                    {
                        if (textchannel.__roleId != 0)
                            await Context.Guild.GetRole(textchannel.RoleId).DeleteAsync();           // Delete roles that are attached to the text channel.
                        await Context.Guild.GetChannel(textchannel.TextChannelId).DeleteAsync();
                    }

                    /// Discord Role deletion.
                    foreach (var ranksetting in channelService.RankSettingList)
                        await Context.Guild.GetRole(ranksetting.RoleId).DeleteAsync();
                    
                    await Context.Guild.GetChannel(channelService.MainCategoryId).DeleteAsync();     // Delete Main category.
                    await Context.Guild.GetChannel(channelService.GameCategoryId).DeleteAsync();     // Delete Game category.
                    await Context.Guild.GetChannel(channelService.SkillCategoryId).DeleteAsync();    // Delete Skill category.

                    /// Database deletion.
                    long longGuildId = Context.Guild.Id.ToLong();

                    var dbChannelService = context.TextChannelSettings                               // Get database ChannelService entry.
                        .Where(c => c.__guildChannelId == longGuildId)
                        .Include(t => t.TextChannelList)
                        .Include(r => r.RankSettingList)
                        .FirstOrDefault();
                    context.TextChannels.RemoveRange(dbChannelService.TextChannelList);              // Removes database TextChannel entries.
                    context.RankSettings.RemoveRange(dbChannelService.RankSettingList);              // Removes database RankSetting entries.

                    context.TextChannelSettings.Remove(dbChannelService);                            // Remove database ChannelService entry.

                    context.SaveChanges();
                }
            }
            LocalStorage.RemoveChannelService(Context.Guild.Id);
            await ReplyAsync("", false, EmbedBuilderService.GetText(
                "SERVER MESSAGE",
                "Removed all roles and game-related channels from the server.",
                "Thank You for playing! #FoxieCleanUp")
                .WithCurrentTimestamp()
                .Build());
        }

        /// <summary>
        /// Creates Textchannel async.
        /// </summary>
        /// <param name="type">TextChannelType enum.</param>
        /// <param name="categoryChannel">Category channel.</param>
        /// <returns>RestTextChannel object.</returns>
        private async Task<RestTextChannel> CreateTextChannelAsync(TextChannelType type, RestCategoryChannel categoryChannel = null)
        {
            var textChannel = await Context.Guild.CreateTextChannelAsync(type.ToString());

            if (categoryChannel != null)
                await SetCategory(textChannel, categoryChannel);
            return textChannel;
        }
        
        /// <summary>
        /// Creates ReadOnly Textchannel.
        /// </summary>
        /// <param name="type">TextChannelType enum.</param>
        /// <param name="categoryChannel">Category channel.</param>
        /// <returns>RestTextChannel object.</returns>
        private async Task<RestTextChannel> CreateReadOnlyChannelAsync(TextChannelType type, RestCategoryChannel categoryChannel = null)
        {
            var textChannel = await Context.Guild.CreateTextChannelAsync(type.ToString());
            await textChannel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, OverwritePermissionsExt.ReadOnly);

            if (categoryChannel != null)
                await SetCategory(textChannel, categoryChannel);
            return textChannel;
        }

        /// <summary>
        /// Creates a Role-locked Textchannel.
        /// </summary>
        /// <param name="type">TextChannelType enum.</param>
        /// <param name="categoryChannel">Category channel.</param>
        /// <returns>Tuple(TextChannelType, RestTextChannel, RestRole)</returns>
        private async Task<Tuple<TextChannelType, RestTextChannel, RestRole>> CreateRoleLockedTextChannelAsync(TextChannelType type, RestCategoryChannel categoryChannel = null)
        {
            var textChannel = await CreateTextChannelAsync(type, categoryChannel);
            RestRole role = await CreateRoleAsync(type.ToString(), GuildPermissionsExt.ReadSendHistoryMention, Color.LightGrey, false);
            await textChannel.AddPermissionOverwriteAsync(role, OverwritePermissionsExt.ReadSendHistoryMention);
            await textChannel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, OverwritePermissionsExt.DenyAll);

            return new Tuple<TextChannelType, RestTextChannel, RestRole>(type, textChannel, role);
        }

        /// <summary>
        /// Creates Role async.
        /// </summary>
        /// <param name="roleName">Role name.</param>
        /// <param name="permissions">GuildPermissions.</param>
        /// <param name="color">Color.</param>
        /// <param name="isHoisted">IsHoisted.</param>
        /// <returns>RestRole object.</returns>
        private async Task<RestRole> CreateRoleAsync(string roleName, GuildPermissions permissions, Color color, bool isHoisted)
        {
            return await Context.Guild.CreateRoleAsync(roleName, permissions, color, isHoisted);
        }

        /// <summary>
        /// Creates category channel.
        /// </summary>
        /// <param name="categoryName">Category name.</param>
        /// <returns>RestCategoryChannel object.</returns>
        private async Task<RestCategoryChannel> CreateCategoryChannelAsync(string categoryName)
        {
            return await Context.Guild.CreateCategoryChannelAsync(categoryName);
        }
        
        /// <summary>
        /// Sets Textchannel's parent (category).
        /// </summary>
        /// <param name="textchannel">Textchannel.</param>
        /// <param name="category">Categorychannel.</param>
        private Task SetCategory(RestTextChannel textchannel, RestCategoryChannel category)
        {
            return textchannel.ModifyAsync(t => t.CategoryId = category.Id);
        }

        /// <summary>
        /// Allows you to reassign respective channels manually in case a sneaky turtle deleted one channel. *OOPS*
        /// *Note: Make sure to manually recreate the role properties if the deleted channel has requirements. #feelsbadman GL.
        /// </summary>
        /// <param name="name">TextChannelType enum.</param>
        [Command("setchannelto -y")]
        [Summary("Assigns/sets current channel to a chose (enum) TextChannelType.")]
        public async Task SetChannelTo(string name)
        {
            if (Enum.TryParse(name, true, out TextChannelType channelType))
            {
                using (HeresonDbContext context = new HeresonDbContext())
                {
                    var channelService = LocalStorage.FindChannelService(Context.Guild.Id);
                    if (channelService != null)
                    {
                        /// Database update.
                        long longGuildId = Context.Guild.Id.ToLong();

                        var dbChannelService = context.TextChannelSettings      // Get database ChannelService entry.
                            .Where(c => c.__guildChannelId == longGuildId)
                            .Include(t => t.TextChannelList)
                            .FirstOrDefault();

                        // Local commit.
                        var currentLocalTextChannel = channelService.FindTextChannel(channelType);
                        currentLocalTextChannel.ChangeTextChannelId(Context.Channel.Id);

                        // Database commit.
                        var currentTextChannel = dbChannelService.FindTextChannel(channelType);
                        currentTextChannel.ChangeTextChannelId(Context.Channel.Id);

                        context.Entry(dbChannelService).CurrentValues.SetValues(currentTextChannel);

                        context.SaveChanges();
                    }
                }
                await ReplyAsync("", false, EmbedBuilderService.GetText(
                    "SERVER MESSAGE",
                    $"The current channel **{Context.Channel.Name}** is now set to the **{channelType}** channel.")
                    .WithCurrentTimestamp()
                    .Build());
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (TextChannelType channel in Enum.GetValues(typeof(TextChannelType)))
                    sb.AppendLine(" * " + channel.ToString());
                await ReplyAsync("", false, EmbedBuilderService.GetErrorText($"Unable to parse channelname **{name}**. " +
                    $"The following channels are available:\n\n{sb.ToString()}").Build());
            }
        }
    }
}