﻿using Discord.Commands;
using Discord.WebSocket;
using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Users;
using HeresonBot.Extensions.Discord.Preconditions;
using HeresonBot.Services.Database;
using System.Threading.Tasks;

namespace HeresonBot.Modules
{
    /// <summary>
    /// Class that is used for debugging/cheating. TODO?: RequireUserPermission(Discord.GuildPermission.Administrator)
    /// </summary>
    [IsHereson]
    public class CheatModule : ModuleBase<SocketCommandContext>
    {
        /// <summary>
        /// Adds achievement.
        /// </summary>
        /// <param name="name">Achievement name.</param>
        [Command("addachi")]
        public async Task AddAchievement([Remainder]string name)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterAchievementsAsync(context, Context.User.Id);
                Achievement achievement = await HeresonDbService.GetAchievementAsync(context, name);
                if (achievement != null)
                {
                    if (character.AchievementService.AddAchievementReference(achievement))
                    {
                        await context.SaveChangesAsync();
                        await ReplyAsync($"Added achievement: {achievement.Name}\n{achievement.Description}");
                    }
                    else
                        await ReplyAsync($"You've already obtained that the achievement: {achievement.Name}.");
                }
                else
                    await ReplyAsync($"Unable to find achievement: {name}.");
            }
        }

        /// <summary>
        /// Removes achievement.
        /// </summary>
        /// <param name="name">Achievement name.</param>
        [Command("removeachi")]
        public async Task RemoveAchievement([Remainder]string name)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterAchievementsAsync(context, Context.User.Id);

                if (character.AchievementService.RemoveAchievement(name))
                {
                    await context.SaveChangesAsync();
                    await ReplyAsync($"Removed achievement.");
                }
                else
                    await ReplyAsync($"Unable to find achievement: {name}.");
            }
        }

        /// <summary>
        /// Adds item to inventory.
        /// </summary>
        /// <param name="item">Quantity.</param>
        /// <param name="itemName">Item name.</param>
        [Command("additem")]
        public async Task AddItemToInven(int quantity, [Remainder] string itemName)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                Character character = await HeresonDbService.GetCharacterInventoryAsync(context, Context.User.Id);
                Item item = await HeresonDbService.GetItemAsync(context, itemName);
                if (item != null)
                {
                    if (character.InventoryService.AddItemToInventory(item, quantity))
                    {
                        await context.SaveChangesAsync();
                        await ReplyAsync($"Added **{item.Name}** (x{quantity}).");
                    }
                    else
                        await ReplyAsync($"Inventory full (max number of slots is 32).");
                }
                else
                    await ReplyAsync($"Unable to find {itemName}.");
            }
        }

        /// <summary>
        /// Changes your username.
        /// </summary>
        [Command("hectogons")]
        [Summary("Add hectogons.")]
        public async Task AddHectogons(SocketGuildUser socketGuildUser, int amount)
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                User user = await HeresonDbService.GetUserAsync(context, socketGuildUser.Id);
                user.Hectogons = amount;
                await context.SaveChangesAsync();
                await ReplyAsync($"{Context.Client.GetUser(user.DiscordId).Username} received {amount} hectogons.");
            }
        }

        /// <summary>
        /// Deletes yourself (User object).
        /// When you hate yourself... :')
        /// </summary>
        [Command("deletemyself")]
        public async Task DeleteMyAss()
        {
            using (HeresonDbContext context = new HeresonDbContext())
            {
                context.Users.Remove(await HeresonDbService.GetUserAsync(context, Context.User.Id));
                await context.SaveChangesAsync();
                await ReplyAsync("removed ..");
            }
        }
    }
}
