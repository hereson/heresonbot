﻿using Discord.Commands;
using Discord.WebSocket;
using HeresonBot.DataClasses.Users;
using HeresonBot.Services.Database;
using HeresonBot.Extensions.Discord.Preconditions;
using HeresonBot.Services.Party;
using System.Text;
using System.Threading.Tasks;

namespace HeresonStudioBot.Modules
{
    /// <summary>
    /// Party Module.
    /// Note: The Party Module is working, but does not find a use yet in the game. 
    /// I've disabled it by removing the keyword 'public' from the class so that it's not added to the commands list.
    /// This implementation was intended for minigames such as 'Exploding Kittens' and 'Quiz' (etc).
    /// These minigames are implemented in a different solution (previous version of the bot).
    /// </summary>
    [IsPartyUserCreated]
    class PartyModule : ModuleBase<SocketCommandContext>
    {
        /// <summary>
        /// Creates party.
        /// </summary>
        [Command("createparty")]
        [Alias("create")]
        [Summary("Creates a new party.")]
        public async Task CreateParty()
        {
            PartyUser user = LocalStorage.GetUser(Context.User.Id);
            if (user.InParty)
                await ReplyAsync($"Unable to create party when you're already in a party.");
            else
            {
                user.CreatePartyService();
                await ReplyAsync($"Party created by {Context.User.Username}. Type -join {Context.User.Mention} to join this party.");
            }
        }

        /// <summary>
        /// Joins party.
        /// </summary>
        /// <param name="socketGuildUser">@hereson</param>
        [Command("joinparty")]
        [Alias("join")]
        [Summary("Joins an existing party.")]
        public async Task JoinParty([Remainder] SocketGuildUser socketGuildUser)
        {
            PartyUser host = LocalStorage.FindUser(socketGuildUser.Id);
            if (host == null)
                await ReplyAsync($"This player is not hosting a party.");
            else
            {
                PartyService partyService = host.GetParty();

                if (partyService == null)
                    await ReplyAsync($"{socketGuildUser.Username} is not hosting a party.");
                else
                {
                    if (partyService.Count >= 8)
                        await ReplyAsync($"This party is full.");
                    else
                    {
                        PartyUser user = LocalStorage.GetUser(Context.User.Id);
                        if (user.IsEqualTo(host))
                            await ReplyAsync($"You cannot join your own party.");
                        else
                        {
                            if (partyService.AddUser(user))
                                await ReplyAsync($"Joined {socketGuildUser.Username}'s party.");
                            else
                                await ReplyAsync($"You're already in a party.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Leaves party.
        /// </summary>
        [Command("leaveparty")]
        [Alias("leave")]
        [Summary("Leaves current party.")]
        public async Task LeaveParty()
        {
            PartyUser user = LocalStorage.GetUser(Context.User.Id);
            PartyService party = user.GetParty();
            if (party == null)
                await ReplyAsync($"You're not in a party.");
            else
            {
                party.RemoveUser(user);
                if (user.IsEqualTo(party.Host))
                {
                    if (party.Count == 0)
                        await ReplyAsync($"{Context.User.Username} left. No players in party. Party disbanded.");
                    else
                    {
                        party.SetNewHost(party.UserList[0]);
                        await ReplyAsync($"{Context.User.Username} left. Host transferred to: {Context.Client.GetUser(party.Host.DiscordId).Username}.");
                    }
                }
                else
                    await ReplyAsync($"Left party...");
            }
        }

        /// <summary>
        /// Disbands party.
        /// </summary>
        [Command("disbandparty")]
        [Alias("disband")]
        [Summary("Disbands your party.")]
        public async Task DisbandParty()
        {
            PartyUser user = LocalStorage.GetUser(Context.User.Id);
            PartyService party = user.GetParty();
            if (party == null)
                await ReplyAsync($"You're not in a party.");
            else
            {
                if (user.IsEqualTo(party.Host))
                {
                    party.Disband();
                    await ReplyAsync($"{Context.User.Username} disbanded the party.");
                }
                else
                    await ReplyAsync($"Failed to disband party. You're not the host.");
            }
        }

        /// <summary>
        /// Shows party info for testing purposes.
        /// TODO: Remove (this is used for debugging).
        /// </summary>
        [Command("showpartyinfo")]
        public async Task ShowPartyInfo()
        {
            PartyUser user = LocalStorage.GetUser(Context.User.Id);
            PartyService party = user.GetParty();
            if (party != null)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < party.Count; i++)
                    sb.AppendLine($"{Context.Client.GetUser(party.UserList[i].DiscordId)} - InParty: {party.UserList[i].InParty}");
                sb.AppendLine($"Host: {Context.Client.GetUser(party.Host.DiscordId).Username}\n");
                await ReplyAsync(sb.ToString());
            }
        }
    }
}