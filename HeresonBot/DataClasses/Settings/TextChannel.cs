﻿using HeresonBot.DataClasses.Types;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Settings
{
    class TextChannel
    {
        [Key]
        public long __textChannelId { get; set; }

        public TextChannelType TextChannelType { get; set; }
        
        public long __roleId { get; set; }

        public TextChannel() { }

        public TextChannel(TextChannelType channelType, ulong textChannelId,  ulong roleId = 0)
        {
            TextChannelId = textChannelId;
            RoleId = roleId;
            TextChannelType = channelType;
        }

        /// <summary>
        /// Changes TextChannel Id.
        /// </summary>
        /// <param name="textChannelId">TextChannel Id.</param>
        public void ChangeTextChannelId(ulong textChannelId)
        {
            TextChannelId = textChannelId;
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set TextchannelId.
        /// </summary>
        public ulong TextChannelId
        {
            get { unchecked { return (ulong)__textChannelId; } }
            set { unchecked { __textChannelId = (long)value; } }
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set RoleId.
        /// </summary>
        public ulong RoleId
        {
            get { unchecked { return (ulong)__roleId; } }
            set { unchecked { __roleId = (long)value; } }
        }

        /// <summary>
        /// Gets name of ChannelType.
        /// </summary>
        /// <returns>String.</returns>
        public string GetChannelTypeName()
        {
            return TextChannelType.ToString();
        }
    }
}