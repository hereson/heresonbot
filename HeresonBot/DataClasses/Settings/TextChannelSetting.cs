﻿using HeresonBot.DataClasses.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Settings
{
    class TextChannelSetting
    {
        [Key]
        public long __guildChannelId { get; set; }
        
        public long __mainCategoryId { get; set; }
        
        public long __gameCategoryId { get; set; }
        
        public long __skillCategoryId { get; set; }

        public List<TextChannel> TextChannelList { get; set; } 

        public List<RankSetting> RankSettingList { get; set; }

        public TextChannelSetting() { }

        public TextChannelSetting(ulong guildChannelId, ulong mainCategoryId, ulong gameCategoryId, ulong skillCategoryId,
            List<RankSetting> rankSettingList)
        {
            this.GuildChannelId = guildChannelId;

            this.MainCategoryId = mainCategoryId;
            this.GameCategoryId = gameCategoryId;
            this.SkillCategoryId = skillCategoryId;

            RankSettingList = rankSettingList;
            TextChannelList = new List<TextChannel>();
        }

        /// <summary>
        /// Finds RankSetting object.
        /// </summary>
        /// <param name="rank">Rank enum.</param>
        /// <returns>RankSetting object.</returns>
        public RankSetting FindRankSetting(Rank rank)
        {
            foreach (RankSetting r in RankSettingList)
                if (r.Rank == rank)
                    return r;
            return null;
        }

        /// <summary>
        /// Finds TextChannel based on Id.
        /// </summary>
        /// <param name="textChannelId">TextChannel Id.</param>
        /// <returns>TextChannel object.</returns>
        public TextChannel FindTextChannel(ulong textChannelId)
        {
            foreach (TextChannel ctc in TextChannelList)
                if (ctc.TextChannelId == textChannelId)
                    return ctc;
            return null;
        }

        /// <summary>
        /// Finds TextChannel based on ChannelType.
        /// </summary>
        /// <param name="channelType">TextChannelType enum.</param>
        /// <returns>TextChannel object.</returns>
        public TextChannel FindTextChannel(TextChannelType channelType)
        {
            foreach (TextChannel ctc in TextChannelList)
                if (ctc.TextChannelType == channelType)
                    return ctc;
            return null;
        }

        /// <summary>
        /// Finds TextChannel based on Id.
        /// </summary>
        /// <param name="type">TextChannelType enum.</param>
        /// <param name="textChannelId">TextChannel Id.</param>
        /// <param name="roleId">Role Id.</param>
        /// <returns>TextChannel object.</returns>
        public bool AddTextChannel(TextChannelType type, ulong textChannelId, ulong roleId)
        {
            if (FindTextChannel(textChannelId) == null)
            {
                TextChannelList.Add(new TextChannel(type, textChannelId, roleId));
                return true;
            }
            return false;
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set Guild Id.
        /// </summary>
        public ulong GuildChannelId
        {
            get { unchecked { return (ulong)__guildChannelId; } }
            set { unchecked { __guildChannelId = (long)value; } }
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set MainCategory Id.
        /// </summary>
        public ulong MainCategoryId
        {
            get { unchecked { return (ulong)__mainCategoryId; } }
            set { unchecked { __mainCategoryId = (long)value; } }
        }


        [NotMapped]
        /// <summary>
        /// Gets/Set GameCategory Id.
        /// </summary>
        public ulong GameCategoryId
        {
            get { unchecked { return (ulong)__gameCategoryId; } }
            set { unchecked { __gameCategoryId = (long)value; } }
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set SkillCategory Id.
        /// </summary>
        public ulong SkillCategoryId
        {
            get { unchecked { return (ulong)__skillCategoryId; } }
            set { unchecked { __skillCategoryId = (long)value; } }
        }
    }
}