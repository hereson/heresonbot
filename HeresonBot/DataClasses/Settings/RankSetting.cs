﻿using HeresonBot.DataClasses.Types;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Settings
{
    /// <summary>
    /// Dataclass that maps the Rank to a Discord Role.
    /// </summary>
    class RankSetting
    {
        [Key]
        public long RankSettingId { get; set; }
        
        /// <summary>
        /// Role Id.
        /// </summary>
        public long __roleId { get; set; }

        /// <summary>
        /// Rank Enum.
        /// </summary>
        public Rank Rank { get; set; }

        public RankSetting() { }

        public RankSetting(Rank rank, ulong roleId)
        {
            Rank = rank;
            RoleId = roleId;
        }

        [NotMapped]
        /// <summary>
        /// Gets/Set RoleId.
        /// </summary>
        public ulong RoleId
        {
            get { unchecked { return (ulong)__roleId; } }
            set { unchecked { __roleId = (long)value; } }
        }
    }
}