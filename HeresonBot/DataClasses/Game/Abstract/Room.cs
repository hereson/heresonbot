﻿using HeresonBot.Services.Game;
using System.Collections.Generic;

namespace HeresonBot.DataClasses.Game.Abstract
{
    abstract class Room
    {
        public List<Room> RoomList = new List<Room>();

        public string RoomName { get; set; }

        public bool IsRoomLocked = false;

        public Room(string roomName = "")
        {
            RoomName = roomName;
        }

        public void AddRoom(Room room)
        {
            if (room != null)
                RoomList.Add(room);
        }

        public virtual string GetDescription(string name, string username, string gender = "He" /* Capitalize */)
        {
            return $"{username} entered the {name}. {gender} seemed uninterested.";
        }

        public Room GetNextRoom()
        {
            if (RoomList.Count == 0)
                return null;
            return RoomList[RandomService.GetNumber(RoomList.Count)];
        }
    }
}