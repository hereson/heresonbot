﻿using HeresonBot.DataClasses.Types;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.DataClasses.Game.Abstract
{
    abstract class Item
    {
        [Key]
        public long ItemId { get; set; }

        /// <summary>
        /// Item Name.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Item Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Item Price.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Item Icon.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Item Quality.
        /// </summary>
        public ItemQuality ItemQuality { get; set; }

        /// <summary>
        /// Gets lower-cased Item name.
        /// </summary>
        public string GetLowerName() { return Name.ToLower(); }

        public Item() { }

        public Item(string name, string description, int price, ItemQuality itemQuality, string icon)
        {
            Name = name;
            Description = description;
            Icon = icon;
            Price = price;
            ItemQuality = itemQuality;
        }

        /// <summary>
        /// Uses the item.
        /// </summary>
        /// <param name="args">Optional parameters.</param>
        /// <returns>String.</returns>
        public abstract string Use(params string[] args);

        /// <summary>
        /// ToString() override.
        /// </summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return $"{Name} - {Description} - Value: {Price}.";
        }
    }
}