﻿using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Game;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Game.Abstract
{
    abstract class Unit
    {
        [Key]
        public long UnitId { get; set; }
        
        public long __discordId { get; set; }

        [NotMapped]
        /// <summary>
        /// Gets/sets DiscordId.
        /// </summary>
        public ulong DiscordId
        {
            get { unchecked { return (ulong)__discordId; } }
            set { unchecked { __discordId = (long)value; } }
        }

        /// <summary>
        /// Unit name.
        /// </summary>     
        public string Name { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Rank.
        /// </summary>
        public Rank Rank { get; set; }

        /// <summary>
        /// AttributeService.
        /// </summary>
        public virtual AttributeService AttributeService { get; set; }
        public long? AttributeServiceId { get; set; }
        
        public Unit() { }

        public Unit(string name, string description, Rank rank = Rank.F)
        {
            Name = name;
            Description = description;
            Rank = rank;
            AttributeService = new AttributeService(this);
        }
    }
}