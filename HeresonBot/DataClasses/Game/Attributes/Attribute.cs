﻿using HeresonBot.DataClasses.Game.Items;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Game;
using System;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.DataClasses.Game.Attributes
{
    class Attribute
    {
        [Key]
        public long AttributeId { get; set; }

        public AttributeService AttributeService { get; set; }
        public long AttributeServiceId { get; set; }

        /// <summary>
        /// AttributeType enum.
        /// </summary>
        public AttributeType AttributeType { get; set; }

        /// <summary>
        /// Value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Multiplier.
        /// </summary>
        public double Multiplier { get; set; }

        public Attribute() { }

        public Attribute(AttributeService attributeService, AttributeType attributeType, int value, double multiplier)
        {
            AttributeService = attributeService;
            AttributeType = attributeType;
            Value = value;
            Multiplier = multiplier;
        }

        /// <summary>
        /// Add number to attribute value if it doesn't exceed base stats.
        /// </summary>
        /// <param name="baseAttribute">The base stats.</param>
        /// <param name="dice">Dice object.</param>
        /// <returns>Integer.</returns>
        public int? AddValue(Attribute baseAttribute, Dice dice)
        {
            if (Value + dice.Sides > baseAttribute.Value * 10)
                return null;

            int number = dice.Roll();
            Value = Math.Min(baseAttribute.Value * 10, Value + number);
            return number;
        }

        /// <summary>
        /// Get total value for this attribute.
        /// </summary>
        /// <returns>Double.</returns>
        public double GetTotal()
        {
            return Value * (Multiplier + 1);
        }

        /// <summary>
        /// Get the percentage of attribute upgrades.
        /// 50 means it's halfway upgraded. 100 = fully upgraded.
        /// </summary>
        /// <param name="baseAttribute">Base Attribute object.</param>
        /// <returns>0-100.</returns>
        public int GetUpgradePercentage(Attribute baseAttribute)
        {
            if (baseAttribute.Value == 0)
                return 0;
            return (int)(Value / ((double)baseAttribute.Value * 10) * 100);
        }
    }
}