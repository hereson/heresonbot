﻿using HeresonBot.Services.Game;
using System;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.DataClasses.Game.Achievements
{
    class Achievement
    {
        [Key]
        public long AchievementId { get; set; }
        
        /// <summary>
        /// Name of the achievement.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of the achievement.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets lower-cased achievement name.
        /// </summary>
        public string GetLowerName() { return Name.ToLower(); }

        public Achievement() { }

        public Achievement(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}