﻿using HeresonBot.Services.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HeresonBot.DataClasses.Game.Achievements
{
    class AchievementReference
    {
        [Key]
        public long AchievementReferenceId { get; set; }

        /// <summary>
        /// Achievement service.
        /// </summary>
        public virtual AchievementService AchievementService { get; set; }
        public long AchievementServiceId { get; set; }

        /// <summary>
        /// Achievement object.
        /// </summary>
        public Achievement Achievement { get; set; }
        public long AchievementId { get; set; }

        /// <summary>
        /// The date that we added the achievement.
        /// </summary>
        public DateTime DateCreated { get; set; }

        public AchievementReference() { }

        public AchievementReference(AchievementService achievementService, Achievement achievement)
        {
            AchievementService = achievementService;
            Achievement = achievement;
        }
    }
}