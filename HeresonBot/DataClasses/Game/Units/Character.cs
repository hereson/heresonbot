﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Achievements;
using HeresonBot.DataClasses.Types;
using HeresonBot.DataClasses.Users;
using HeresonBot.Services.Game;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Game.Units
{
    class Character : Unit
    {
        public User User { get; set; }

        /// <summary>
        /// The date that we added the user.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// InventoryService.
        /// </summary>
        public virtual InventoryService InventoryService { get; set; }

        public virtual AchievementService AchievementService { get; set; }

        [NotMapped]
        public virtual AttributeService SkillService { get; set; }
         
        [NotMapped]
        private AttributeService BaseAttributeService;

        public Character() { }

        public Character(User user, string name, string description, Rank rank = Rank.F)
            : base(name, description, rank)
        {
            User = user;
            InventoryService = new InventoryService(this);
            AchievementService = new AchievementService(this);

            BaseAttributeService = new AttributeService(this);
            BaseAttributeService.AddAttribute(AttributeType.HP, 200);
            BaseAttributeService.AddAttribute(AttributeType.Stamina, 100);
            BaseAttributeService.AddAttribute(AttributeType.Recovery, 10);
            BaseAttributeService.AddAttribute(AttributeType.Damage, 10);
            AttributeService.AddValueToAttribute(AttributeType.HP, null);
            //AttributeService.AddAttribute(AttributeType.Critical, 10);
            //AttributeService.AddAttribute(AttributeType.Accuracy, 10);
            //AttributeService.AddAttribute(AttributeType.Armor, 0);
            //AttributeService.AddAttribute(AttributeType.Barrier, 0);
            //AttributeService.AddAttribute(AttributeType.Dodge, 0);


            //AttributeService.AddAttribute(AttributeType.Fishing, 0);
            //AttributeService.AddAttribute(AttributeType.Cooking, 0);
            //AttributeService.AddAttribute(AttributeType.Herbalism, 0);
        }
    }
}