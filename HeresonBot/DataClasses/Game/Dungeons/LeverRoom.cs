﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.Services.Game;

namespace HeresonBot.DataClasses.Game.Dungeons
{
    class LeverRoom : Room
    {
        public LeverRoom(string name, string description)
        {
            IsRoomLocked = true;
        }
    }
}