﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.Services.Game;

namespace HeresonBot.DataClasses.Game.Dungeons
{
    class EmptyRoom : Room
    {
        public EmptyRoom()
        {
            RoomName = GetName();
        }

        public string GetName()
        {
            string[] names = new string[20]
            {
                "corridor",
                "corridor",
                "corridor",
                "hallway",
                "side entrance",
                "side entrance",
                "corridor",
                "long corridor",
                "corridor",
                "small corridor",
                "small corridor",
                "tiny corridor",
                "corridor",
                "funny-looking corridor",
                "scary-looking corridor",
                "scary corridor",
                "corridor",
                "corridor",
                "side entrance",
                "corridor",
            };
            return names[RandomService.GetNumber(names.Length)];
        }

        public override string GetDescription(string name, string username, string gender)
        {
            string[] names = new string[20]
            {
                $"A {name} could be seen from a distance..",
                $"Another {name}..! ",
                $"Another {name}.. ",
                $"Another {name}... ",
                $"That's a huge... Nvm, it's a {name}.",
                $"Walking through the {name}..",
                $"{username} heard some footsteps through {name}.",
                $"{username} heard something... It's coming from {name}...",
                $"Slowly approaching the {name}...",
                $"{username} toss up a coin, {gender} headed left.",
                $"{username} turned around, {gender} strafed right.",
                $"{username} closed its eyes, {gender} walked up north straight into {name}.",
                $"{username} closed its eyes, {gender} walked down south for no reason.",
                $"{username} turned around, {gender} headed down south.",
                $"{username} spun around, {gender} headed down south, but actually headed north.",
                $"{username} spun around, {gender} headed down north, but actually headed south.",
                $"Another {name}... ",
                $"Another {name}.. ",
                $"Another {name}...{username} will not give up though.",
                $"Another {name}?! {username} felt bored.",
            };
            return names[RandomService.GetNumber(names.Length)];
        }
    }
}