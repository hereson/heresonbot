﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Game;
using HeresonBot.DataClasses.ReadOnly;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Game.Items
{
    class Dice : Item
    {
        [NotMapped]
        public int Sides;

        public Dice() : base() { }

        public Dice(int sides, string name, string description, ItemQuality itemQuality) : base(name, description, 1, itemQuality, Icons.ICON_DICE)
        {
            Sides = sides;
        }

        public override string Use(params string[] args)
        {
            int number = Roll();
            return $"Rolled: {Icons.ICON_DICE}{number}";
        }

        /// <summary>
        /// Roll the die and return the number rolled.
        /// </summary>
        /// <returns>Integer.</returns>
        public int Roll()
        {
            return RandomService.GetNumber(Sides) + 1;
        }
    }
}
