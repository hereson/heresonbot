﻿using HeresonBot.DataClasses.Types;

namespace HeresonBot.DataClasses.Game.Items.DiceObjects
{
    class ElementDice : Dice
    {
        public ElementDice() : base(6, "Element Dice", "A die that can be rolled into one of the 6 elements.", ItemQuality.Epic)
        {
        }
    }
}