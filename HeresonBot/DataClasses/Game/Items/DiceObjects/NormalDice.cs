﻿using HeresonBot.DataClasses.Types;

namespace HeresonBot.DataClasses.Game.Items.DiceObjects
{
    class NormalDice : Dice
    {
        public NormalDice()
            :base(6, "Dice", "A plain die with 6 sides. I wonder what will happen when I roll this?", ItemQuality.Rare)
        {

        }
    }
}