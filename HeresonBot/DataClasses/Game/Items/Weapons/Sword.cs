﻿using HeresonBot.DataClasses.Items;
using HeresonBot.DataClasses.Types;

namespace HeresonBot.DataClasses.Game.Items.Weapons
{
    class Sword : Equipment
    {
        public Sword()
            : base("Sword", "A sword.", 1, ItemQuality.Common, EquipmentSlot.MainHand, ElementType.None, ":crossed_swords:")
        {
        }
    }
}