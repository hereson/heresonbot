﻿using System.ComponentModel.DataAnnotations;
using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.Services.Game;

namespace HeresonBot.DataClasses.Items
{
    class ItemReference
    {
        [Key]
        public long ItemReferenceId { get; set; }

        /// <summary>
        /// Inventory service.
        /// </summary>
        public virtual InventoryService InventoryService { get; set; }
        public long InventoryServiceId { get; set; }

        /// <summary>
        /// Item object.
        /// </summary>
        public virtual Item Item { get; set; }
        public long ItemId { get; set; }
        
        /// <summary>
        /// AttributeService.
        /// </summary>
        public virtual AttributeService AttributeService { get; set; }
        public long? AttributeServiceId { get; set; }

        /// <summary>
        /// Quantity.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// IsEquipped.
        /// </summary>
        public bool IsEquipped { get; set; }

        public ItemReference() { }

        public ItemReference(InventoryService inventoryService, Item item, int quantity, bool isEquipped = false)
        {
            InventoryService = inventoryService;
            Item = item;
            Quantity = quantity;
            IsEquipped = isEquipped;
            AttributeService = new AttributeService(this);
        }
    }
}