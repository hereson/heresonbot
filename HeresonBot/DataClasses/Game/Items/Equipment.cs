﻿using HeresonBot.DataClasses.Game.Abstract;
using HeresonBot.DataClasses.Game.Attributes;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Game;

namespace HeresonBot.DataClasses.Items
{
    class Equipment : Item
    {
        /// <summary>
        /// Equipment slot.
        /// </summary>
        public EquipmentSlot Slot { get; set; }

        /// <summary>
        /// Element Type.
        /// </summary>
        public ElementType Element { get; set; }

        /// <summary>
        /// Equipment Rank.
        /// </summary>
        public Rank Rank { get; set; }

        public AttributeService AttributeService { get; set; }
        public long? AttributeServiceId { get; set; }

        public Equipment() : base() { }
        
        public Equipment(string name, string description, int value,
            ItemQuality itemQuality, 
            EquipmentSlot slot,
            ElementType element,
            string icon)
            : base(name, description, value, itemQuality, icon)
        {
            Slot = slot;
            Element = element;
            AttributeService = new AttributeService(this);
            Rank = Rank.F;
        }

        public override string Use(params string[] args)
        {
            return "Equipment cannot be used. To equip the item, use: -equip <itemname>";
        }

        public void ComputeRank()
        {
            if (AttributeService != null)
            {
                int sum = 0;
                foreach (Attribute attribute in AttributeService.AttributeList)
                    sum += attribute.GetUpgradePercentage(attribute);
                double score = sum / AttributeService.AttributeList.Count;

                if (score <= 15)
                    Rank = Rank.F;
                else if (score <= 25)
                    Rank = Rank.E;
                else if (score <= 40)
                    Rank = Rank.D;
                else if (score <= 60)
                    Rank = Rank.C;
                else if (score <= 80)
                    Rank = Rank.B;
                else if (score < 100)
                    Rank = Rank.A;
                else
                    Rank = Rank.S;
            }
        }
    }
}