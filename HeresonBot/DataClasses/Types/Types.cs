﻿using Discord;

namespace HeresonBot.DataClasses.Types
{
    /// <summary>
    /// Helper functions for the different Type enums.
    /// </summary>
    class Types
    {
        /// <summary>
        /// Corresponds to Ranks enum.
        /// </summary>
        public static readonly Color[] RankColors
            = new Color[7] { Color.DarkRed, Color.DarkOrange, Color.Orange, Color.Gold, Color.Purple, Color.Blue, Color.Green };

        /// <summary>
        /// Gets color from rank.
        /// </summary>
        /// <param name="rank">Rank enum.</param>
        /// <returns>Discord Color object.</returns>
        public static Color GetColorFromRank(Rank rank)
        {
            return Types.RankColors[(int)rank];
        }
    }

    /// <summary>
    /// Ranks.
    /// </summary>
    public enum Rank { S, A, B, C, D, E, F }

    /// <summary>
    /// ChannelTypes.
    /// </summary>
    public enum TextChannelType
    {
        ServerAnnouncements, Leaderboard, Town, TownShop, Character, Merchant,
        Fishing, Woodcutting, Mining, Herbalism,
        Cooking, Crafting, Smithing, Alchemy
    };

    /// <summary>
    /// AttributeTypes.
    /// </summary>
    public enum AttributeType
    {
        HP, Stamina, Recovery, Speed,
        Damage, Critical, Accuracy,
        Armor, Barrier, Dodge, Stealth, LifeDrain,
        Fishing, Woodcutting, Mining, Herbalism,
        Cooking, Crafting, Smithing, Alchemy
    }

    /// <summary>
    /// ElementType.
    /// </summary>
    public enum ElementType
    {
        None,
        Lightning, Ice, Flame, Earth, Dark, Soul,
        Plasma, Crystal, Inferno, Nature, Shadow, Spirit
    }

    /// <summary>
    /// Item Quantlity.
    /// </summary>
    public enum ItemQuality { Unknown, Common, Rare, Epic, Legendary, Unique }

    /// <summary>
    /// EquipmentSlots.
    /// </summary>
    public enum EquipmentSlot { MainHand, OffHand, Head, Body, Legs, Shoes, TwoHanded }

    /// <summary>
    /// Characterstates.
    /// </summary>
    public enum CharacterState { Online, InSkill, InInstance }

    /// <summary>
    /// Gender.
    /// </summary>
    public enum Gender { Male, Female }

    #region Party
    /// <summary>
    /// PartyUserStates.
    /// </summary>
    public enum PartyUserState { Idle, InMiniGame }
    #endregion
}