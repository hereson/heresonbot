﻿using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Party;

namespace HeresonBot.DataClasses.Users
{
    class PartyUser
    {
        /// <summary>
        /// PartyService.
        /// </summary>
        private PartyService _partyService;

        /// <summary>
        /// Discord Id.
        /// </summary>
        public ulong DiscordId { get; }
        
        /// <summary>
        /// PartyUser state.
        /// </summary>
        public PartyUserState State { get; set; } = PartyUserState.Idle;

        public PartyUser(ulong discordId)
        {
            DiscordId = discordId;
        }
        
        /// <summary>
        /// Returns the party if the PartyUser is in a party.
        /// </summary>
        /// <returns>PartyService object.</returns>
        public PartyService GetParty()
        {
            if (InParty)
                return _partyService;
            return null;
        }

        /// <summary>
        /// Create party.
        /// </summary>
        public void CreatePartyService()
        {
            _partyService = new PartyService(this);
        }

        /// <summary>
        /// Join party.
        /// </summary>
        public void JoinParty(PartyService partyService)
        {
            _partyService = partyService;
        }

        /// <summary>
        /// Leave party.
        /// </summary>
        public void LeaveParty()
        {
            _partyService = null;
        }

        /// <summary>
        /// Returns true if the PartyUser is in a party.
        /// </summary>
        public bool InParty
        {
            get { return _partyService != null; }
        }

        /// <summary>
        /// Compares two PartyUsers.
        /// </summary>
        /// <param name="user">PartyUser object.</param>
        /// <returns>Boolean.</returns>
        public bool IsEqualTo(PartyUser user)
        {
            return DiscordId == user.DiscordId;
        }
    }
}