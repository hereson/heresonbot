﻿using HeresonBot.DataClasses.Game.Units;
using HeresonBot.DataClasses.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeresonBot.DataClasses.Users
{
    class User
    {
        [Key]
        public long __discordId { get; set; }

        [NotMapped]
        /// <summary>
        /// Gets/sets DiscordId.
        /// </summary>
        public ulong DiscordId
        {
            get { unchecked { return (ulong)__discordId; } }
            set { unchecked { __discordId = (long)value; } }
        }
        /// <summary>
        /// Hectogons.
        /// </summary>
        public int Hectogons { get; set; }

        /// <summary>
        /// The date that we added the user.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Character List.
        /// </summary>
        public virtual List<Character> CharacterList { get; set; } = new List<Character>();
        
        //public virtual Character CurrentActiveCharacter;

        public User() { }

        public User(ulong discordId, string name, int hectogons)
        {
            DiscordId = discordId;
            Hectogons = hectogons;
            CharacterList.Add(new Character(this, name, "An adventurous turtle.", Rank.F)); // TODO, move this out of user (into a function). Factory stuff...
        }

        public Character GetCharacter()
        {
            return CharacterList[0]; // Only allow one character for now.
        }
    }
}