﻿namespace HeresonBot.DataClasses.ReadOnly
{
    class Icons
    {
        #region GeneralCommands
        public static readonly string ICON_ACHIEVEMENTS = ":ribbon:";
        public static readonly string ICON_STATS = ":bar_chart:";
        public static readonly string ICON_ENCHANTMENTS = ":trident:";
        #endregion

        #region Elements
        public static readonly string ICON_FIRE = ":fire:";
        public static readonly string ICON_ICE = ":snowflake:";
        public static readonly string ICON_EARTH = ":shamrock:";
        public static readonly string ICON_LIGHTNING = ":zap:";
        public static readonly string ICON_SOUL = ":skull:";
        public static readonly string ICON_DARK = ":eight_pointed_black_star:";
        #endregion

        #region Dice
        public static readonly string ICON_DICE = ":game_die:";
        #endregion
    }
}