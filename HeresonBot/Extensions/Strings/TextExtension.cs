﻿using System;
using System.Text.RegularExpressions;

namespace HeresonBot.Extensions.Strings
{
    static class TextExtension
    {
        public static readonly string ERROR_ALPHANUMERIC_ONLY_32_CHAR_MAX = "Alphanumeric characters only, no spaces (max 32 characters).";
        public static readonly string ERROR_CHARACTER_NOT_FOUND = "Hmmm, that's weird. We haven't been able to find this character.";

        /// <summary>
        /// Formats string percentage.
        /// 0.8625 becomes 86%.
        /// </summary>
        /// <param name="val">Value.</param>
        /// <returns>String.</returns>
        public static string ToPercentage(this double val)
        {
            return String.Format("{0:0%}", val);
        }

        /// <summary>
        /// Removes all white spaces from a string.
        /// </summary>
        /// <param name="str">String.</param>
        /// <returns>String.</returns>
        public static string RemoveWhiteSpaces(this string str)
        {
            return Regex.Replace(str, @"\s+", "");
        }

        /// <summary>
        /// Checks if a string contains the following alphanumeric characters.
        /// </summary>
        /// <param name="str">String.</param>
        /// <returns>Boolean.</returns>
        public static bool ContainsAlphaNumericCharacters(this string str)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (r.IsMatch(str))
                return true;
            return false;
        }

        /// <summary>
        /// Checks if a string contains the following alphanumeric characters and punctuation.
        /// </summary>
        /// <param name="str">String.</param>
        /// <returns>Boolean.</returns>
        public static bool ContainsAlphaNumericCharactersAndPunctuation(this string str)
        {
            Regex r = new Regex("^[a-zA-Z0-9 \"!?.:;)(^_-]*$");
            if (r.IsMatch(str))
                return true;
            return false;
        }
    }
}