﻿namespace HeresonBot.Extensions.Numerics
{
    static class UlongExtension
    {
        /// <summary>
        /// Converts ulong to long.
        /// </summary>
        public static long ToLong(this ulong id)
        {
            unchecked { return (long)id; }
        }
    }
}