﻿using Discord;

namespace HeresonBot.Extensions.Discord.Channels
{
    class GuildPermissionsExt
    {
        public static readonly GuildPermissions ReadSendHistoryMention
            = new GuildPermissions(false, false, false, false, false, false, false, false,
                true, true,                 // Read, Send.
                false, false, false, false,
                true, true);                // History, Mention Everyone.
    }
}