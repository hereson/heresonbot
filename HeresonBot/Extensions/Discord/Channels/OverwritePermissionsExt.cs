﻿using Discord;

namespace HeresonBot.Extensions.Discord.Channels
{
    class OverwritePermissionsExt
    {
        public static readonly OverwritePermissions ReadOnly
            = new OverwritePermissions(PermValue.Deny, PermValue.Deny, PermValue.Deny,
                PermValue.Allow, PermValue.Deny,                                // Read.
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny,
                PermValue.Allow, PermValue.Deny,                                // History.
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny);

        public static readonly OverwritePermissions ReadSendHistoryMention 
            = new OverwritePermissions(PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                PermValue.Allow, PermValue.Allow,                               // Read, Send.
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                PermValue.Allow, PermValue.Allow,                               // History, Mention Everyone.
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny);

        public static readonly OverwritePermissions DenyAll
            = new OverwritePermissions(PermValue.Deny, PermValue.Deny, PermValue.Deny,
                PermValue.Deny, PermValue.Deny,
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny,
                PermValue.Deny, PermValue.Deny,
                PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny, PermValue.Deny);
    }
}