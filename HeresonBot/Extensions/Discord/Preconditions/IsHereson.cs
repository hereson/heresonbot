﻿using Discord.Commands;
using System;
using System.Threading.Tasks;

namespace HeresonBot.Extensions.Discord.Preconditions
{
    public sealed class IsHereson : PreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo command, IServiceProvider services)
        {
            if (Context.User.Id == 186455068176941056)
                return Task.FromResult(PreconditionResult.FromSuccess());
            return Task.FromResult(PreconditionResult.FromError("User is not Hereson."));
        }
    }
}