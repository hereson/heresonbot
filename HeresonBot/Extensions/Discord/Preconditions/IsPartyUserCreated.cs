﻿using Discord.Commands;
using HeresonBot.DataClasses.Users;
using HeresonBot.Services.Database;
using System;
using System.Threading.Tasks;

namespace HeresonBot.Extensions.Discord.Preconditions
{
    public sealed class IsPartyUserCreated : PreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo command, IServiceProvider services)
        {
            if (LocalStorage.ContainsUserId(Context.User.Id))
                return Task.FromResult(PreconditionResult.FromSuccess());
            else
            {
                LocalStorage.AddUser(Context.User.Id, new PartyUser(Context.User.Id));
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
        }
    }
}