﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using HeresonBot.DataClasses.Types;
using HeresonBot.Services.Database;

namespace HeresonBot.Extensions.Discord
{
    public sealed class IsInTextChannel : PreconditionAttribute
    {
        TextChannelType _type { get; }

        public IsInTextChannel(TextChannelType type)
        {
            _type = type;
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext Context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult(PreconditionResult.FromSuccess()); // debug :)

            var textChannel = LocalStorage.FindChannelService(Context.Guild.Id)?.FindTextChannel(_type);

            if (textChannel?.TextChannelId == Context.Channel.Id)
                return Task.FromResult(PreconditionResult.FromSuccess());

            return Task.FromResult(PreconditionResult.FromError("Command not available in this channel."));
        }
    }
}