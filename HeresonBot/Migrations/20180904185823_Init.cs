﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HeresonBot.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Achievements",
                columns: table => new
                {
                    AchievementId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 64, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Achievements", x => x.AchievementId);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    ItemId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemQuality = table.Column<string>(nullable: false),
                    Price = table.Column<int>(nullable: false, defaultValue: 0),
                    Name = table.Column<string>(maxLength: 64, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    Icon = table.Column<string>(maxLength: 64, nullable: true, defaultValue: ""),
                    Discriminator = table.Column<string>(nullable: false),
                    Slot = table.Column<string>(nullable: true),
                    Element = table.Column<string>(nullable: true),
                    Rank = table.Column<string>(nullable: true),
                    AttributeServiceId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.ItemId);
                });

            migrationBuilder.CreateTable(
                name: "TextChannelSettings",
                columns: table => new
                {
                    GuildChannelId = table.Column<long>(nullable: false),
                    MainCategoryId = table.Column<long>(nullable: false),
                    GameCategoryId = table.Column<long>(nullable: false),
                    SkillCategoryId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextChannelSettings", x => x.GuildChannelId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    DiscordId = table.Column<long>(nullable: false),
                    Hectogons = table.Column<int>(nullable: false, defaultValue: 0),
                    DateCreated = table.Column<DateTime>(type: "datetime2(0)", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.DiscordId);
                });

            migrationBuilder.CreateTable(
                name: "RankSettings",
                columns: table => new
                {
                    RankSettingId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<long>(nullable: false, defaultValue: 0L),
                    Rank = table.Column<string>(nullable: false),
                    TextChannelSetting__guildChannelId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RankSettings", x => x.RankSettingId);
                    table.ForeignKey(
                        name: "FK_RankSettings_TextChannelSettings_TextChannelSetting__guildChannelId",
                        column: x => x.TextChannelSetting__guildChannelId,
                        principalTable: "TextChannelSettings",
                        principalColumn: "GuildChannelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TextChannels",
                columns: table => new
                {
                    TextChannelId = table.Column<long>(nullable: false),
                    TextChannelType = table.Column<string>(nullable: false),
                    RoleId = table.Column<long>(nullable: false, defaultValue: 0L),
                    TextChannelSetting__guildChannelId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextChannels", x => x.TextChannelId);
                    table.ForeignKey(
                        name: "FK_TextChannels_TextChannelSettings_TextChannelSetting__guildChannelId",
                        column: x => x.TextChannelSetting__guildChannelId,
                        principalTable: "TextChannelSettings",
                        principalColumn: "GuildChannelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    DiscordId = table.Column<long>(nullable: false),
                    AttributeServiceId = table.Column<long>(nullable: true),
                    Rank = table.Column<string>(nullable: false, defaultValue: "F"),
                    UnitId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2(0)", nullable: true, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.UnitId);
                    table.ForeignKey(
                        name: "FK_Units_Users_DiscordId",
                        column: x => x.DiscordId,
                        principalTable: "Users",
                        principalColumn: "DiscordId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AchievementService",
                columns: table => new
                {
                    AchievementServiceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UnitId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchievementService", x => x.AchievementServiceId);
                    table.ForeignKey(
                        name: "FK_AchievementService_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InventoryService",
                columns: table => new
                {
                    InventoryServiceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UnitId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryService", x => x.InventoryServiceId);
                    table.ForeignKey(
                        name: "FK_InventoryService_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AchievementReferences",
                columns: table => new
                {
                    AchievementReferenceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AchievementServiceId = table.Column<long>(nullable: false),
                    AchievementId = table.Column<long>(nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2(0)", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchievementReferences", x => x.AchievementReferenceId);
                    table.ForeignKey(
                        name: "FK_AchievementReferences_Achievements_AchievementId",
                        column: x => x.AchievementId,
                        principalTable: "Achievements",
                        principalColumn: "AchievementId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AchievementReferences_AchievementService_AchievementServiceId",
                        column: x => x.AchievementServiceId,
                        principalTable: "AchievementService",
                        principalColumn: "AchievementServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemReferences",
                columns: table => new
                {
                    ItemReferenceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InventoryServiceId = table.Column<long>(nullable: false),
                    ItemId = table.Column<long>(nullable: false),
                    AttributeServiceId = table.Column<long>(nullable: true),
                    Quantity = table.Column<int>(nullable: false, defaultValue: 1),
                    IsEquipped = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemReferences", x => x.ItemReferenceId);
                    table.ForeignKey(
                        name: "FK_ItemReferences_InventoryService_InventoryServiceId",
                        column: x => x.InventoryServiceId,
                        principalTable: "InventoryService",
                        principalColumn: "InventoryServiceId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemReferences_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ItemId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AttributeService",
                columns: table => new
                {
                    AttributeServiceId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<long>(nullable: true),
                    UnitId = table.Column<long>(nullable: true),
                    ItemReferenceId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttributeService", x => x.AttributeServiceId);
                    table.ForeignKey(
                        name: "FK_AttributeService_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ItemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttributeService_ItemReferences_ItemReferenceId",
                        column: x => x.ItemReferenceId,
                        principalTable: "ItemReferences",
                        principalColumn: "ItemReferenceId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttributeService_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Attributes",
                columns: table => new
                {
                    AttributeId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttributeServiceId = table.Column<long>(nullable: false),
                    AttributeType = table.Column<string>(nullable: false),
                    Value = table.Column<int>(nullable: false, defaultValue: 0),
                    Multiplier = table.Column<double>(nullable: false, defaultValue: 0.0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attributes", x => x.AttributeId);
                    table.ForeignKey(
                        name: "FK_Attributes_AttributeService_AttributeServiceId",
                        column: x => x.AttributeServiceId,
                        principalTable: "AttributeService",
                        principalColumn: "AttributeServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AchievementReferences_AchievementId",
                table: "AchievementReferences",
                column: "AchievementId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AchievementReferences_AchievementServiceId",
                table: "AchievementReferences",
                column: "AchievementServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AchievementService_UnitId",
                table: "AchievementService",
                column: "UnitId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attributes_AttributeServiceId",
                table: "Attributes",
                column: "AttributeServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeService_ItemId",
                table: "AttributeService",
                column: "ItemId",
                unique: true,
                filter: "[ItemId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeService_ItemReferenceId",
                table: "AttributeService",
                column: "ItemReferenceId",
                unique: true,
                filter: "[ItemReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeService_UnitId",
                table: "AttributeService",
                column: "UnitId",
                unique: true,
                filter: "[UnitId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryService_UnitId",
                table: "InventoryService",
                column: "UnitId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemReferences_InventoryServiceId",
                table: "ItemReferences",
                column: "InventoryServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemReferences_ItemId",
                table: "ItemReferences",
                column: "ItemId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RankSettings_TextChannelSetting__guildChannelId",
                table: "RankSettings",
                column: "TextChannelSetting__guildChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_TextChannels_TextChannelSetting__guildChannelId",
                table: "TextChannels",
                column: "TextChannelSetting__guildChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_Units_DiscordId",
                table: "Units",
                column: "DiscordId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AchievementReferences");

            migrationBuilder.DropTable(
                name: "Attributes");

            migrationBuilder.DropTable(
                name: "RankSettings");

            migrationBuilder.DropTable(
                name: "TextChannels");

            migrationBuilder.DropTable(
                name: "Achievements");

            migrationBuilder.DropTable(
                name: "AchievementService");

            migrationBuilder.DropTable(
                name: "AttributeService");

            migrationBuilder.DropTable(
                name: "TextChannelSettings");

            migrationBuilder.DropTable(
                name: "ItemReferences");

            migrationBuilder.DropTable(
                name: "InventoryService");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
